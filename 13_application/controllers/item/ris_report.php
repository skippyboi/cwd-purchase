<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ris_report extends CI_Controller {

	public function __construct(){
		parent :: __construct();

		$this->load->model('item/md_ris_report');
			
	}

	public function index(){
		$this->lib_auth->title = "RIS Report";		
		$this->lib_auth->build = "item/ris_report";		
		$data['project'] = $this->md_project->get_profit_center();
		$this->lib_auth->render($data);		
	}
	
	public function get_item(){
		if(!$this->input->is_ajax_request()){
			exit(0);
		}



		$arg = $this->input->post();

		$data['pr_items'] = $this->md_ris_report->get_item($arg);
		$this->load->view('item/ris_list',$data);

	}

	public function get_return(){
		if(!$this->input->is_ajax_request()){
			exit(0);
		}

		$arg = $this->input->post();

		$data['pr_items'] = $this->md_ris_report->get_return($arg);
		$this->load->view('item/return_list', $data);
	}

	public function get_direct_receiving(){
		if(!$this->input->is_ajax_request()){
			exit();
		}

		$arg = $this->input->post();

		$data['pr_items'] = $this->md_ris_report->get_receiving($arg);
		$this->load->view('item/direct_receiving_list', $data);
	}

}