<?php defined('BASEPATH') OR exit('No direct script access allowed');

class return_report extends CI_Controller {

	public function __construct(){
		parent :: __construct();

		$this->load->model('item/md_return_report');
			
	}

	public function index(){
		$this->lib_auth->title = "RIS Report";		
		$this->lib_auth->build = "item/return_report";		
		$data['project'] = $this->md_project->get_profit_center();
		$this->lib_auth->render($data);		
	}
	
	public function get_item(){
		if(!$this->input->is_ajax_request()){
			exit(0);
		}
		$arg = $this->input->post();

		$data['pr_items'] = $this->md_return_report->get_item($arg);
		$this->load->view('item/return_report',$data);

	}

}