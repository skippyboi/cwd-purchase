<div class="content-page">
    <div class="content">


<div class="header">
	<h2>Item Setup</h2>
</div>


<div class="container">

<div class="content-title">
	<h3>Setup Item</h3>	
</div>


<input type="hidden" id="id" value="" class="clear">

<div class="row">
	<div class="col-md-5">

		<div class="panel panel-default">		
		  <div class="panel-body">
		  		
		  		<div class="row">
		  			<div class="col-xs-9">
		  				<div class="form-group">
				  			<div class="control-label">Item Group</div>
				  			<select name="" id="group_id" class="form-control">
				  				
				  			</select>
				  			<?php foreach($item_group as $row): ?>
				  					<!-- <option value="<?php echo $row['group_id']; ?>"><?php echo $row['group_description']; ?> </option> -->
				  			<?php endforeach; ?>
				  		</div>
		  			</div>	
		  			<div class="col-xs-3" style="padding:0px">
		  				
		  					<button id="edit_group" class="btn btn-default btn-sm" style="margin-top:13px;display:inline-block" title="edit"><i title="edit" class="fa fa-edit"></i></button>	
		  					<button id="item_group" class="btn btn-default btn-sm" style="margin-top:13px;display:inline-block" title="add"><i title="edit" class="fa fa-plus"></i></button>		  				
		  				
		  				
		  				
		  			</div>		  			

		  		</div>

		  		<div class="form-group">
		  			<div class="control-label">Stock Code</div>
		  			<input type="text" id="item_code" class="form-control input-sm uppercase">
		  		</div>

		  		<div class="form-group">
		  			<div class="control-label">Item Description</div>
		  			<input type="text" id="item_description" class="form-control input-sm required uppercase">
		  		</div>

		  		<div class="row">
		  			<div class="col-xs-12">
		  				<div class="form-group">
					  			<div class="control-label">Unit Measure</div>
					  			<input type="text" id="unit_measure" class="form-control input-sm required uppercase">
					  	</div>		  				
		  			</div>
		  			<div class="col-md-4">
		  				<div class="form-group">
				  			<div class="control-label">Re-Ordering Point</div>
				  			<input type="number" name="" id="reordering" class="form-control input-sm required numbers_only">
			  			</div>
		  			</div>		
		  			<div class="col-md-4">
		  				<div class="form-group">
				  			<div class="control-label">Interval Range</div>
				  			<input type="number" name="" id="interval_range" class="form-control input-sm required numbers_only">
			  			</div>
		  			</div>
		  			<!-- 
		  			<div class="col-xs-6">
			  			<div class="form-group">
				  			<div class="control-label">Item Quantity</div>
				  			<input type="text" id="item_quantity" class="form-control input-sm required uppercase numbers_only">
				  		</div>
		  			</div> 
		  			-->
		  		</div>
		  			<div class="row">
			  			<div class="col-md-4">
			  				<div class="form-group">
			  					<input type="checkbox" id="fast_moving" value="YES" class="control-label">  Fast Moving</input><br/>	
			  					<input type="checkbox" id="for_sale" value="YES" class="control-label">  For Sale??</input>
					  			<!-- <div class="control-label"><>Fast Moving</div> -->
					  			<!-- <input type="text" name="" id="fast_moving" class="form-control input-sm required uppercase"> -->
					  			<!-- <select name="" id="fast_moving" class="form-control">
					  				<option value="">-</option>
					  				<option value="YES">YES</option>
					  				<option value="NO">NO</option>
					  			</select> -->
				  			</div>
			  			</div>
			  		</div>
		  		<!-- <div class="form-group">
		  			<div class="control-label">Account Classification</div>
		  			<select name="" id="account_classification" class="form-control">
		  				<?php foreach($account_classification as $row): ?>
		  					<?php $selected =  ($row['id']=='1')? 'selected="selected"' : ''; ?>
							<option <?php echo $selected ?> value="<?php echo $row['id']; ?>"><?php echo $row['full_description']; ?></option>
		  				<?php endforeach; ?>
		  			</select>
		  		</div> -->
		  		<div class="form-group">
		  			<div class="control-label">Item Type</div>
		  			<select name="" id="item_type" class="form-control">
		  				<option value=""> - </option>
		  				<option value="STOCK" selected="selected">STOCK</option>
		  				<option value="NON-STOCK">NON-STOCK</option>
		  			</select>
		  		</div>
		  		<div class="form-group">
		  			<div class="control-label">Account Name</div>
			  			<!-- <input type="text" name="" id="account_name" style="width:100%" placeholder="Account Name" data-desc="" data-code=""> -->

		  			<select name="" id="account_name" class="select2" style="width:300px;">
		  				<option value="0"> - </option>
		  				<?php
		  					if(count($account_name) > 0){
		  						foreach($account_name as $row){
		  				?>
		  				<option data-classification="<?php echo $row['classification_id'];?>" value="<?php echo $row['id'];?>" data-code="<?php echo $row['account_code'];?>"><?php echo $row['account_description'];?></option>
		  				<?php
		  						}
		  					}
		  				?>
		  			</select>
		  		</div>
  				<div class="form-group">
  					<div class="control-label">Subsidiary</div>
  					<select name="" id="subsid" class="select2" style="width:50%">
  						<option value="">Select Subsidiary</option>
  					</select>
  				</div>
		  </div>

		  <div class="form-footer">
				<div class="row">
					<div class="col-md-7"> </div>
					<div class="col-md-5">
						<input id="class_save" class="btn btn-success col-xs-5 pull-right btn-sm" type="submit" value="Save">						
						<input id="class_reset" class="btn btn-link col-xs-5 pull-right btn-sm" type="submit" value="Reset">						
					</div>
				</div>
		   </div>
		</div>
	
	</div>

	<div class="col-md-7">
		<div class="panel panel-default">		
		  <div class="panel-body">		  		
		  </div>	
		  <div class="classification_setup_content">
		 	 <table class="table table-striped">
				<thead>
					<tr>
						<th>Item No</th>
						<th>Item Category</th>
						<th>Item Description</th>
						<th>Unit Measure</th>
						<th>Action</th>
					</tr>
				</thead>
			 </table>
		  </div> 
		</div>		
	</div>


</div>

</div>

</div>
</div>

<script>	
	var xhr = '';
	var edit_xhr = '';
	var app_sub_classification ={
		init:function(){
						
			$('#group_id').chosen();
			$('.select2').select2();
			// $('#account_name').chosen();

			this.get_classification_setup();
			this.bindEvent();
			this.account_type();
			this.get_items();
			this.get_account_name();

			$('#account_name').on('change',function(){
				$.getJSON('<?php echo site_url('procurement/purchase_request/get_account_subsidiary')?>',{account_id:$('#account_name option:selected').val()},function(json){
	            	$('#subsid option:gt(0)').remove();
	            	$('#subsid option:first').val('0');
	            	$('#subsid option:first').text('Select Item');
	            	for(var i = 0; i < json.length; i++){
	                	$('<option/>').attr('value',json[i]['id']).attr('data-accountid',json[i]['account_id']).attr('data-desc',json[i]['description']).attr('data-id', json[i]['id']).text(json[i]['description']).appendTo($('#subsid')).trigger('change');
	                }
	            });
			});

			

		},get_items:function(group_id){
			if(typeof(group_id) == 'undefined'){
				group_id = '';
			}
			$post = {
				group_id : group_id
			}
			$.post('<?php echo base_url().index_page();?>/setup/item_setup/get_group_item',$post,function(response){
				$('#group_id').html(response);
				$("#group_id").trigger("chosen:updated");
			});
		},get_classification_setup:function(){
			$('.classification_setup_content').content_loader('true');
			$.post('<?php echo base_url().index_page();?>/setup/item_setup/get_data',function(response){
				$('.classification_setup_content').html(response);
				$('.classification_table').dataTable(datatable_option);
			});
		},get_account_name:function(){
			/*$("#account_name").select2({
		     	 	placeholder: "Search Account Here",
		     	 	allowClear: true,
				    ajax: {
				        url: '<?php echo site_url("procurement/purchase_request/get_account_account"); ?>',
				        dataType: 'json',
				        type: "GET",
				        quietMillis: 50,
				        data: function (term) {
				            return {
				                q: term
				            };
				        },				     
				        results: function (data){
				            return {
				                results: $.map(data, function (item) {					                              
				                    return {
				                        text: item.account_description,
				                        id  : item.id,
				                        account_id : item.type_id,
				                        account_code : item.account_code,
				                        account_name : item.account_description,
				                        desc : item.account_description,
				                        account_default : item.t_account,
				                        class_id : item.classification_id
				                    }
				                })
				            };
				        }
				    },initSelection: function (element, callback){
				         	var id = $(element).val();
				         	$(element).val();
					},formatSelection:function(object,container){
						var data = {
		                    item_desc : object.desc,
		                    item_id :object.id,
		                    account_id : object.account_id,
		                    accnt_name : object.account_name,
		                    accnt_code : object.account_code,
		                    accnt_default : object.account_default,
		                    class_id : object.class_id
		                };

		                alert(data.account_id);
		                $('#account_name').val(data.accnt_name);
		                $('#account_name').attr('data-code', data.accnt_code);
		                $('#account_name').attr('data-id', data.account_id);
		                $('#account_name').attr('data-desc', data.item_desc);
		                $('#account_name').attr('data-default', data.accnt_default);
		                $('#account_name').attr('data-classification', data.class_id);

		                $get = {
		                	title: data.item_desc
		                };
		                // $('#subsid').prop('hidden', false);

		                 $.getJSON('<?php echo site_url('procurement/purchase_request/get_account_subsidiary')?>',$get,function(json){
		                	$('#subsid option:gt(0)').remove();
		                	$('#subsid option:first').val('0');
		                	$('#subsid option:first').text('Select Item');
		                	for(var i = 0; i < json.length; i++){
			                	$('<option/>').attr('value',json[i]['id']).attr('data-accountid',json[i]['account_id']).attr('data-desc',json[i]['description']).attr('data-id', json[i]['id']).text(json[i]['description']).appendTo($('#subsid'));
			                }
		                });

		                return object.text;
					}
			});*/

		},bindEvent:function(){

			$('#class_save').on('click',this.class_save);
			$('#class_reset').on('click',this.class_reset);
			$('body').on('click','.update_class',this.update);
			$('body').on('click','.remove_class', this.remove);
			$('#back_to').on('click',this.back_to);
			$('#txtShortDesc').on('change',this.account_type);			
			$('#item_group').on('click',this.item_group);			
			$('#edit_group').on('click',this.edit_group);

			/*$('#account_classification').on('change',function(){
				$post = {
					class_code : $(this).val(),
				}
				$.post('<?php echo base_url().index_page();?>/setup/item_setup/get_account_setup',$post,function(response){
						var div = "";

						$.each(response,function(i,val){
							var selected = "";
							if(val.account_id == '10'){
								selected = "selected='selected'";
							}
							div +="<option "+selected+" value="+val.account_id+">"+val.account_description+"</option>";
						});
						$('#account_name').html(div);
				},'json');
			});
			$('#account_classification').trigger('change');*/

		},class_save:function(){
			var fast_moving = 'NO';
			var for_sale = 'NO';
			var sub_id = $('#subsid option:selected').attr('data-id');
			var sub_name = $('#subsid option:selected').attr('data-desc');

			if($('#subsid option:selected').val() == '0'){
				sub_id = '0';
				sub_name = '';
			}

			if($('#fast_moving').checked = true)
			{
				fast_moving = 'YES';
			}
			if($('#for_sale').checked = true)
			{
				for_sale = 'YES';
			}
			if($('.required').required()){
				return false;
			}
			
			$.save();
			// alert($('#reordering').val());
				// $('#invterval_range').val()$('#fast_moving option:selected').val());
			$post = {
				    id                  : $('#id').val(),
				    code 			    : $('#item_code').val(),
				    group_id            : $('#group_id option:selected').val(),
					description         : $('#item_description').val(),
					quantity            : $('#item_quantity').val(),					
					unit_measure        : $('#unit_measure').val(),
					account_id          : $('#account_name option:selected').val(),
					classification_id   : $('#account_name option:selected').attr('data-classification'),
					account_code      	: $('#account_name option:selected').attr('data-code'),
					type 				: $('#item_type option:selected').val(),
					reordering_point	: $('#reordering').val(),
					interval_range		: $('#interval_range').val(),
					sub_id				: sub_id,
					sub_name			: sub_name,
					fast_moving			: fast_moving,
					is_for_sale			: for_sale
			};

			// alert(for_sale);
			$.post('<?php echo base_url().index_page();?>/setup/item_setup/save_item',$post,function(response){
					switch($.trim(response)){
						case"1": 
							$.save({action : 'success'});
						break;
						default :
							$.save({action : 'hide'});
						break;
					}

				$('.required,.clear').val('');
				$('#item_code').val('');
				app_sub_classification.get_classification_setup();

			}).error(function(){
				alert('Service Unavailable');
			});

			$('#class_save').val('Save');
			$('#reordering').prop('checked', false);
			$('#interval_range').prop('checked', false);
			$('#account_name').val('');

		},update:function(){

			var tr = $(this).closest('tr');
			// alert(tr.find('td.for_sale').text());
			// if(tr.find('td.fast_moving').text() == 'YES' && tr.find('td.for_sale').text() == 'YES')
			// {
			// 	var edit = {
			// 		id                : tr.find('td.id').text(),
			// 		item_code  		  : tr.find('td.item_code').text(),
			// 		group_id          : tr.find('td.group_id').text(),
			// 		item_description  : tr.find('td.description').text(),
			// 		unit_measure      : tr.find('td.unit_measure').text(),
			// 		item_quantity     : tr.find('td.quantity').text(),
			// 		account_name      : tr.find('td.account_id').text(),
			// 		type              : tr.find('td.item_type').text(),
			// 		reordering  	  : tr.find('td.reordering').text(),
			// 		interval_range    : tr.find('td.interval_range').text(),
			// 		fast_moving		  : $('#fast_moving').prop('checked', true),
			// 		for_sale		  : $('#for_sale').prop('checked', true)
			// 	}				
			// }
			
				var edit = {
						id                : tr.find('td.id').text(),
						item_code  		  : tr.find('td.item_code').text(),
						group_id          : tr.find('td.group_id').text(),
						item_description  : tr.find('td.description').text(),
						unit_measure      : tr.find('td.unit_measure').text(),
						item_quantity     : tr.find('td.quantity').text(),
						account_name      : tr.find('td.account_id').text(),
						type              : tr.find('td.item_type').text(),
						reordering  	  : tr.find('td.reordering').text(),
						interval_range    : tr.find('td.interval_range').text(),
						fast_moving		  : tr.find('td.fast_moving').text() == 'YES' ? $('#fast_moving').prop('checked', true) : $('#fast_moving').prop('checked', false),
						for_sale 		  : tr.find('td.for_sale').text() == 'YES' ? $('#for_sale').prop('checked', true) : $('#for_sale').prop('checked', false),
						subid 			  : tr.find('td.subid').text(),
						// for_sale		  : tr.find('td.for_sale').text() == 'YES' ? $('#for_sale').prop('checked', true) : $('#for_sale').prop('checked' false)						
					}								
			// alert(tr.find('td.for_sale').text());
			$.each(edit,function(i,val){
				$('#'+i).val(val);
			});
			
			$("#group_id").trigger("chosen:updated");
			$('#account_name').val(edit.account_name).attr('selected','selected').trigger('change');
			$.getJSON('<?php echo site_url('procurement/purchase_request/get_account_subsidiary')?>',{account_id:$('#account_name option:selected').val()},function(json){
            	$('#subsid option:gt(0)').remove();
            	$('#subsid option:first').val('0');
            	$('#subsid option:first').text('Select Item');
            	for(var i = 0; i < json.length; i++){
            		if(edit.subid == json[i]['id']){
                		$('<option/>').attr('value',json[i]['id']).attr('data-accountid',json[i]['account_id']).attr('data-desc',json[i]['description']).attr('data-id', json[i]['id']).text(json[i]['description']).attr('selected','selected').appendTo($('#subsid')).trigger('change');
                	}else{
                		$('<option/>').attr('value',json[i]['id']).attr('data-accountid',json[i]['account_id']).attr('data-desc',json[i]['description']).attr('data-id', json[i]['id']).text(json[i]['description']).appendTo($('#subsid')).trigger('change');
                	}
                }
            });
			$('#class_save').val('Update');
			// $('#subsid').prop('hidden', true);

		},class_reset:function(){
			$('.required,.clear').val('');
			$('#item_code').val('');
			$('#class_save').val('Save');
		},back_to:function(){
			$.save({appendTo : '.fancybox-outer',loading : 'Processing...'});
			
			$.post('<?php echo base_url().index_page();?>/setup/account_setup/new_request',function(response){
				$.fancybox(response,{
					width     : 1200,
					height    : 550,
					fitToView : false,
					autoSize  : false,
				})
			}).error(function(){
				alert('Service Unavailable');
			});

		},account_type:function(){

			$post = {
				account_type : $('#txtShortDesc option:selected').val(),
			};

			$.post('<?php echo base_url().index_page();?>/setup/account_setup/get_classification',$post,function(response){
						$('#cmbClassification').select({
							json : response,
							attr : {
								text : 'full_description',
								value : 'id',
							}
						});			
			},'json');

		},item_group:function(){

			$.fancybox.showLoading();
	        if(xhr && xhr.readystate != 4){
	            xhr.abort();
	        }

			xhr = $.post('<?php echo base_url().index_page();?>/setup/item_setup/item_group',function(response){
				$.fancybox(response,{
					width     : 200,
					height    : 200,
					fitToView : false,
					autoSize  : true,
				})
			});

		},edit_group:function(){

			$.fancybox.showLoading();
	        if(edit_xhr && edit_xhr.readystate != 4){
	            edit_xhr.abort();
	        }
	        $post = {
	        	item_group : $('#group_id option:selected').val(),
	        	item_group_name : $('#group_id option:selected').text()
	        }
			edit_xhr = $.post('<?php echo base_url().index_page();?>/setup/item_setup/edit_group',$post,function(response){
				$.fancybox(response,{
					width     : 200,
					height    : 200,
					fitToView : false,
					autoSize  : true,
				})
			});

		},remove:function(){
			var bool = confirm('Are you Sure?');
			if(!bool){
				return false;
		}

		var me = $(this);
		var group_detail_id = me.closest('tr').find('td.id').text();
		var index = me.closest('tr').get(0);

		$post = {
			group_detail_id : group_detail_id
		};

		$.post('<?php echo base_url().index_page();?>/setup/item_setup/delete',$post,function(response){
				switch($.trim(response)){
					case "1":
						alert('Successfully Remove!');
						app_sub_classification.get_classification_setup();						
					break;
					case "default":
						alert('Something went Wrong');
					break;
				}
			});
		}
	};

	$(function(){		
		app_sub_classification.init();
	});
</script>

