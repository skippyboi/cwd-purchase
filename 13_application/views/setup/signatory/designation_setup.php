<div class="content-page">
 <div class="content">

<div class="header">
	<h2>Designation Setup</h2>
</div>

<div class="container">

				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">

								<div class="panel panel-default">		
								  <div class="panel-body">
								  		<div class="form-group">
								  			<div class="control-label">Designation Name</div>
								  			<input type="text" id="designation_name" class="form-control input-sm required uppercase">
								  		</div>
								  </div>

								  <div class="form-footer">
										<div class="row">
											<div class="col-md-7"> </div>
											<div class="col-md-5">
												<input id="class_save" class="btn btn-success col-xs-5 pull-right btn-sm" type="submit" value="Save">						
											</div>
										</div>
								   </div>
								</div>
							
							</div>

							<div class="col-md-8">
								<div class="panel panel-default">		
								  <div class="panel-body">		  		
								  </div>	
								  <div class="classification_setup_content">
								 	 <table class="table table-striped">
										<thead>
											<tr>
												<th>Type</th>						
											</tr>
										</thead>
									 </table>
								  </div> 
								</div>		
							</div>


						</div>

						</div>
					</div>
				</div>				
</div>

</div>
</div>

<script>
	$(function(){

		var app = {
			init:function(){
				this.get_classification_setup();
				this.bindEvent();
			},get_classification_setup:function(){
				$('.classification_setup_content').content_loader('true');
				$.post('<?php echo base_url().index_page();?>/setup/signatory/get_designation',function(response){
					$('.classification_setup_content').html(response);
					$('.classification_table').dataTable(datatable_option);
				});
			},bindEvent:function(){

				$('body').on('click','.remove_class',this.remove);
				$('#class_save').on('click',this.class_save);
				// $('#addtolist').on('click',this.addtolist);
				// $('#form').on('change',this.signatory_list);
				// $('#signatory').on('change',this.signatory_list);

				// $('body').on('click','.remove',this.remove);

				// $('#form').trigger('change');

			},remove:function(){
			var bool = confirm('Are you Sure?');
			if(!bool){
				return false;
			}

			var me = $(this);
			var designation_id = me.closest('tr').find('td.id').text();
			var index = me.closest('tr').get(0);

			$post = {
				designation_id : designation_id
			};

			$.post('<?php echo base_url().index_page();?>/setup/signatory/delete',$post,function(response){
					switch($.trim(response)){
						case "1":
							alert('Successfully Remove!');
							app.get_classification_setup();						
						break;
						case "default":
							alert('Something went Wrong');
						break;
					}
				});
			},class_save:function(){

				if($('.required').required()){
					return false;
				}
				
				$.save();

				$post = {
						designation_name  : $('#designation_name').val()
				};
				alert($post['designation_name']);
				$.post('<?php echo base_url().index_page();?>/setup/signatory/save_designation',$post,function(response){
						switch($.trim(response)){
							case"1": 
								$.save({action : 'success'});
							break;
							default :
								$.save({action : 'hide'});
							break;
						}

					$('.required,.clear').val('');
					app.get_classification_setup();

				}).error(function(){
					alert('Service Unavailable');
				});

				$('#class_save').val('Save');

			},update:function(){

				var tr = $(this).closest('tr');

				var edit = {
					id              : tr.find('td.id').text(),
					designation_name    : tr.find('td.designation_name').text(),
					status : tr.find('td.status').text(),
				}

				
				$.each(edit,function(i,val){
					$('#'+i).val(val);
				});
				
				$('#class_save').val('Update');

			}

		};
		$(function(){
			app.init();
		});

	});	
</script>