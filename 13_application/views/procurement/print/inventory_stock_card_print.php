<div id="wrapper" style="width:900px">
<div class="container">
	<div style="height:30px;"></div>

	<!-- <p  style="text-align: center;">SUPPLIES LEDGER CARD<br>
	 CAR-CAR WATER DISTRICT<br>
	 CARCAR CITY, CEBU CITY</p> -->
	<p style="text-align: center;">
		<span class="center-text bold" style="display:block;"><?php echo $header['title']; ?></span>
		<span class="center-text" style="display:block;"><?php echo $header['address']; ?></span>
		<span class="center-text bold" style="font-size:18px;display:block;">INVENTORY STOCK CARD</span>
	</p>
	<table id="item-table">
		<tbody><!-- 
			<tr class="border">
				<th colspan="7" rowspan="3">
					<span class="center-text bold" style="display:block;"><?php echo $header['title']; ?></span>
					<span class="center-text" style="display:block;"><?php echo $header['address']; ?></span>
					<span class="center-text bold" style="font-size:18px;display:block;">MOVING AVERAGE</span>
				</th>
			</tr> -->
			<tr>
				<td>AS OF:</td>
				<td colspan="4"></td>
				<td></td>
			</tr>
			<tr>
				<td>ITEM:</td>
				  <td colspan="4"><span><strong> <font style="text-decoration:underline;"><?php echo $item['group_description'];?></font></span></strong></td>
				  <td>STOCK NO: <span><strong> <font style="text-decordation:underline;"><?php echo $item['item_code'];?></font></strong></span>					</td>
			</tr>
			<tr>
				<td>DESCRIPTION</td>
				<td colspan="4"><span><strong> <font style="text-decoration:underline;"><?php echo $item['description'];?></font></span></strong>
				</td>
				<td >RE-ORDER POINT  ___________</td>
				<!-- <td>  ___________</td>
				<td></td> -->
			</tr>
			<tr style="text-align:center;">
		  		<td rowspan="2">DATE</td>
		  		<td rowspan="2">REFERENCE</td>
		  		<td>RECEIPT</td>
		  		<td>RETURN</td>
		  		<td>ISSUANCE</td>
		  		<td >BALANCE</td>
		  		<!-- <td colspan="3">BALANCE</td> -->
		 	 </tr>
		 	  <tr class="center-text">
		  		<td class="center-text">QTY</td>
		  		<!-- <td>UNIT COST</td> -->
		  		<!-- <td></td> -->
		  		<!-- <td>TOTAL COST</td> -->
		  		<td>QTY</td>
		  		<!-- <td>UNIT COST</td> -->
		  		<!-- <td></td> -->
		  		<!-- <td>TOTAL COST</td> -->
		  		<td class="center-text">QTY</td>
		  		<!-- <td></td> -->
		  		<!-- <td>TOTAL COST</td> -->
		  		<td>QTY</td>
		  		<!-- <td>QTY</td>
		  		<td>UNIT COST</td>
		  		<td>TOTAL COST</td> -->
		  </tr>
		  <?php
				if($item_list > 0){
					$balance = 0;
					foreach($item_list as $row){
						if($row['type'] == 'DIRECT RECEIVING'){
                          	$balance = $balance + $row['debit'];
                        }elseif($row['type'] == 'WITHDRAW'){
                          	$balance = $balance - $row['credit'];
                        }elseif($row['type'] == 'TRANSFER'){
                          	$balance = $balance - $row['credit'];
                        }elseif($row['type'] == 'WITHDRAW'){
                          	$balance = $balance - $row['credit'];
                        }elseif($row['type'] == 'RETURN'){
                        	$balance = $balance + $row['debit'];
                        }elseif($row['type'] == 'BEGINNING'){
                        	$balance = $balance + $row['debit'];
                        }elseif($row['type'] == 'RIS'){
                        	$balance = $balance - $row['credit'];
                        }
			?>
			<tr class="border">
				<?php $total_cost_balance = $balance * $row['unit_cost'];
				$total_cost = $row['qty'] * $row['unit_cost'];	?>
				<td class="center-text"><?php echo date('m/d/Y',strtotime($row['trans_date']));?></td>
				<td class="center-text"><?php echo $row['reference_no'];?></td>
				<?php if($row['type'] == 'DIRECT RECEIVING' || $row['type'] == 'RECEIVING'){ ?>
				<td class="center-text"><?php echo number_format($row['debit']); ?></td>
				<!-- <td class="center-text"><?php echo $row['debit']; ?></td> -->
				<!-- <td class="center-text"><?php echo $row['unit_cost']; ?></td> -->
				<!-- <td></td> -->
				<!-- <td class="center-text"><?php echo $row['total_cost']; ?></td> -->
			<?php }else{?>
				<td class="center-text"></td>
				<!-- <td class="center-text"></td> -->
				<!-- <td class="center-text"></td> -->
			<?php }?>
			<?php if($row['type'] == 'RETURN'){
				 ?>
				 <?php if($row['debit'] != 0){?>
				<td class="center-text"><?php echo number_format($row['debit']); ?></td>
			<?php } else {?>
				<td class="center-text"><?php echo number_format($row['credit']); ?></td>
			<?php }?>
				<!-- <td class="center-text"><?php echo $row['debit']; ?></td> -->
				<!-- <td class="center-text"><?php echo $row['unit_cost'];?></td> -->
				<!-- <td></td> -->
				<!-- <td class="center-text"><?php echo $row['total_cost'];?></td> -->
				<?php }
				else{
					?>
				<td class="center-text"></td>
				<!-- <td class="center-text"></td> -->
				<!-- <td class="center-text"></td> -->
					<?php
				}?>	
				<?php if($row['type'] == 'RIS' || $row['type'] == 'RETURN TO SUPPLIER'){?>
				<td class="center-text"><?php echo number_format($row['credit']);?></td>
				<!-- <td></td> -->
				<!-- <td class="center-text"><?php echo $total_cost; ?></td> -->
			<?php }else{?>
				<td class="center-text"></td>
				<!-- <td class="center-text"></td> -->
			<?php }?>
				<td class="center-text"><?php echo number_format($row['balance']);?></td>
				<!-- <td class="center-text"><?php echo $row['balance']?></td> -->
				<!-- <td class="center-text"><?php $unit_cost = $row['bal_total_cost'] / $row['balance']; echo round($unit_cost,2);?></td>
				<td class="center-text"><?php echo $row['bal_total_cost'];?></td>
				 --><!-- // balance total on the database - add column -->
			</tr>
			<?php
					}
				}
			?>
			<!-- <tr class="border">
				<td colspan="2">
					<span style="display:block;margin-top:-15px;"><strong>Items:</strong> <font style="text-decoration:underline;"><?php echo $item['group_description'];?></font>_________</span>
				</td>
				<td colspan="3">
					<span style="display:block;margin-top:-15px;"><strong>Description:</strong> <font style="text-decoration:underline;"><?php echo $item['description'];?></font>_________</span>
				</td>
				<td colspan="2">
					<span style="display:block;border-bottom:1px solid #999;"><strong>Stock No.:</strong> <font style="text-decordation:underline;"><?php echo $item['group_detail_id'];?></font></span>
					<span style="display:block;"><strong>Reorder Point:</strong> ___________</span>
				</td>
			</tr>
			<tr class="border">
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th class="center-text">Receipt</th>
				<th colspan="2" class="center-text">ISSUANCE</th>
				<th class="center-text">Balance</th>
				<th class="center-text">&nbsp;</th>
			</tr>
			<tr class="border">
				<th class="center-text">DATE</th>
				<th class="center-text">REFERENCE</th>
				<th class="center-text">QTY</th>
				<th class="center-text">QTY</th>
				<th class="center-text">OFFICE</th>
				<th class="center-text">&nbsp;</th>
				<th class="center-text">NO. OF DAYS TO CONSUME</th>
			</tr>
			<?php
				if(count($item_list) > 0){
					$balance = 0;
					foreach($item_list as $row){
						if($row['type'] == 'RECEIVING'){
                          	$balance = $balance + $row['debit'];
                        }elseif($row['type'] == 'WITHDRAW'){
                          	$balance = $balance - $row['credit'];
                        }elseif($row['type'] == 'TRANSFER'){
                          	$balance = $balance - $row['credit'];
                        }elseif($row['type'] == 'WITHDRAW'){
                          	$balance = $balance - $row['credit'];
                        }elseif($row['type'] == 'RETURN'){
                        	$balance = $balance + $row['debit'];
                        }elseif($row['type'] == 'BEGINNING'){
                        	$balance = $balance + $row['debit'];
                        }
			?>
			<tr class="border">
				<td class="center-text"><?php echo date('m/d/Y',strtotime($row['trans_date']));?></td>
				<td class="center-text"><?php echo $row['reference_no'];?></td>
				<td class="center-text"><?php echo $row['debit'];?></td>
				<td class="center-text"><?php echo $row['credit'];?></td>
				<td class="center-text"><?php echo $row['office'];?></td>
				<td class="center-text"><?php echo $balance;?></td>
				<td>&nbsp;</td>
			</tr>
			<?php
					}
				}
			?> -->
			<tr class="border">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>

				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
			</tr>
			<tr class="border">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
			</tr>
			<tr class="border">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
			</tr>
			<tr class="border">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
			</tr>
			<tr class="border">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
				<!-- <td>&nbsp;</td> -->
			</tr>
		</tbody>
	</table>
	
	<div class="print_ft" style="margin-top:10px;">
		<div class="row">
			<div class="col-md-4"><span class="bold">FM-WHS-06</span></div>
			<div class="col-md-4" style="text-align:center;"><span class="bold">00</span></div>
			<div class="col-md-4" style="text-align:right;"><span class="bold">8/20/2016</span></div>
		</div>
		<div class="row">
			<div style="float: left;">
				<div class="col-md-12">Prepared by:</div>
				<div class="col-md-12"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JESSA BACON</strong></div>
			</div>
			<div style="float: right;">
				<div class="col-md-12">Verified by:</div>
				<div class="col-md-12" style="text-align:right;"><strong>CAMIA CAMPUGAN</strong></div>
			</div>
		</div>
	</div>

</div>
</div>
		
<script>

</script>
	