<div class="panel panel-warning">   
    <div class="panel-heading">JOURNAL ENTRIES</div>                        
    <table class="table table-striped table-responsive">
        <thead>
            <tr>
                <th style="width:20px;"></th>
                <th style="width:100px;">CODE</th>
                <th style="width:400px"> ACCOUNT TITLE</th>
                <th style="width:400px"> SUBSIDIARY</th>
                <th> DEBIT</th>
                <th> CREDIT</th>
                <th style="display:none;">TYPE</th>
                <th style="width:10%;display:none;">DEFAULT</th>
            </tr>               
        </thead>
        <tbody class="bill_journal_contents">
            
        </tbody>                            
        <tfoot>
            <tr>
                <td></td>
                <td><a href="javascript:void(0)" class="add_row_journal">add row</a></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="display:none;"></td>
                <td style="display:none;"></td>
            </tr>
        </tfoot>                            
        <tfoot>
            <tr >
                <td class="border" colspan="4">
                    <span class="pull-right">TOTAL AMOUNT</span>
                </td>
                <td class="border total_amount total_debit" style="font-weight:bold;font-size:15px">0.00</td>
                <td class="border total_amount total_credit" style="font-weight:bold;font-size:15px">0.00</td>
                <td style="display:none;"></td>
                <td style="display:none;"></td>
            </tr>
        </tfoot>
    </table>
</div>

<script>
$(document).ready(function(){
    //start journal entries
    $.editable.addInputType('select2',{
        element : function(settings, original){
            var select = $('<select />').addClass('select2');
            $(this).append(select);                  
            return(select);
        },
        content : function(data, settings, original) {
            

            /* If it is string assume it is json. */
            if (String == data.constructor){
                eval ('var json = ' + data);
            } else {
            /* Otherwise assume it is a hash already. */
                var json = data;
            }
            for (var key in json) {
                if (!json.hasOwnProperty(key)) {
                    continue;
                }
                if ('selected' == key) {
                    continue;
                }

                if(json[key].hasOwnProperty('account_description')){
                  var option = $('<option />').val(json[key]['account_description']).append(json[key]['account_description']);
                  if(json[key].hasOwnProperty('attr'))
                  {
                    option.attr(json[key].attr);
                  }
                }else{
                  var option = $('<option />').val(key).append(json[key]);
                }                        
                $('select', this).append(option);                        
            }
            /* Loop option again to set selected. IE needed this... */ 
            $('select', this).children().each(function(){
                if ($(this).val() == json['selected'] || 
                    $(this).text() == $.trim(original.revert)) {
                        $(this).attr('selected', 'selected');
                }
            });

            /* Submit on change if no submit button defined. */
             $('select', this).select2({ width: "500px"});
                                        
            if (!settings.submit) {
                var form = this;
                $('select', this).change(function() {
                    form.submit();                            
                });
            }                    
        }
    });

    $('body').on('click','.remove',function(){
        var debit = $(this).closest('tr').find('.debit').text();
        var credit = $(this).closest('tr').find('.credit').text();
        var total_debit = $('.total_debit').text();
        var total_credit = $('.total_credit').text();

        $(this).closest('tr').get(0).remove();
        debit = debit.replace(',','');
        credit = credit.replace(',','');
        total_debit = total_debit.replace(',','');
        total_credit = total_credit.replace(',','');

        var totaldebit = 0;
        var totalcredit = 0;

        if(parseFloat(debit) > 0 && parseFloat(credit) == 0){
            totaldebit = parseFloat(total_debit) - parseFloat(debit);

            $('.total_debit').text(comma(round2Fixed(totaldebit)));
        }else if(parseFloat(credit) > 0 && parseFloat(debit) == 0){
            totalcredit = parseFloat(total_credit) - parseFloat(credit);

            $('.total_credit').text(comma(round2Fixed(totalcredit)));
        }

        if(parseFloat($('.total_debit').text()) != parseFloat($('.total_credit').text())){
            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:red;');
            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:red;');
        }

        if(parseFloat($('.total_debit').text()) == parseFloat($('.total_credit').text())){
            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:blue;');
            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:blue');
        }

    });


    //for editable start
    $('.editable_journal').editable(function(value,settings,data){                  
            var id = $(data).find(':selected').attr('data-account_id');
            $(this).attr('data-account_id',id);

            var ledger = $(data).find(':selected').attr('data-ledger');
            $(this).attr('data-ledger',ledger);

            var code = $(data).find(':selected').attr('data-code');
            $(this).attr('data-code',code);

            $(this).closest('tr').find('td.editable_code').text(code);

            var t_account = $(data).find(':selected').attr('data-taccount');
            $(this).attr('data-taccount',t_account);
            $(this).closest('tr').find('td.default').text(t_account).attr('style','color:blue;display:none;');

            if(t_account == 'DEBIT'){
                $(this).closest('tr').find('td:eq(4)').attr('style','color:blue;');
                $(this).closest('tr').find('td:eq(5)').removeClass('pointer');
            }

            if(t_account == 'CREDIT'){
                $(this).closest('tr').find('td:eq(4)').removeClass('pointer');
                $(this).closest('tr').find('td:eq(5)').attr('style','color:blue;');
            }

            return value;

        },{
         loadurl  : '<?php echo site_url('procurement/purchase_request/get_account_title'); ?>',                   
         placeholder : 'Select Account title',
         type        : "select2",
         submit      : 'ok',
         callback    : function(){
            var id = $(this).attr('data-account_id');

            $div = '<select class="subsidiary_select">'
                +'<option value=""></option>'
                +'</select>';
            $(this).parent().children('.editable_subsidiary').html($div);

            var select_subs = $(this).parent().find('.subsidiary_select');

            $.getJSON('<?php echo site_url('procurement/purchase_request/get_subsidiary');?>',{id:id},function(json){
                
                $(this).parent().children('.subsidiary_select option:gt(0)').remove();
                $(this).parent().children('.subsidiary_select option:first').text('');
                if(json.length > 0){
                    for(var i = 0; i < json.length; i++){
                        if(json[i]['account_id'] == id){
                            $('<option/>').attr('value',json[i]['id']).text(json[i]['description']).appendTo(select_subs);
                        }
                    }
                }
            });
         }
    });

    $('body').on('change','.debit',function(){
        var debit = $(this).closest('tr').find('.debit').val();

        get_total();    
    });

    $('body').on('change','.credit',function(){
        var credit = $(this).closest('tr').find('.credit').val();

        get_total();      
    });


    $('.add_row_journal').on('click',function(){

        $div =  '<tr>' 
                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
                +'<td class="editable_code pointer"></td>'
                +'<td class="editable_journal pointer" data-account_id="" data-ledger="" data-code=""></td>'
                +'<td class="editable_subsidiary pointer" data-subsidiary_id=""></td>'
                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
                +'<td style="display:none;"></td>'
                +'<td class="default" style="display:none;"></td>'
                +'</tr>';               
        $('.bill_journal_contents').append($div);


        journal_editable();

    });

    var get_total = function(){

        var total_debit  = 0;
        var total_credit = 0;
        $('.bill_journal_contents tr').each(function(i,val){
                var debit  = remove_comma($(val).closest('tr').find('.debit').val());
                var credit = remove_comma($(val).closest('tr').find('.credit').val());
                total_debit  = +total_debit + +debit;
                total_credit = +total_credit + +credit;
        });

        $('.total_debit').html(comma(round2Fixed(total_debit)));
        $('.total_credit').html(comma(round2Fixed(total_credit)));

        if(parseFloat($('.total_debit').val()) != parseFloat($('.total_credit').val())){
            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:red;');
            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:red;');
        }else if(parseFloat($('.total_debit').val()) == parseFloat($('.total_credit').val())){
            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:blue;');
            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:blue');
        }

    }
    //end journal entries
});

function journal_editable(){
    //for editable start
    $('.editable_journal').editable(function(value,settings,data){                  
            var id = $(data).find(':selected').attr('data-account_id');
            $(this).attr('data-account_id',id);

            var ledger = $(data).find(':selected').attr('data-ledger');
            $(this).attr('data-ledger',ledger);

            var code = $(data).find(':selected').attr('data-code');
            $(this).attr('data-code',code);

            $(this).closest('tr').find('td.editable_code').text(code);

            var t_account = $(data).find(':selected').attr('data-taccount');
            $(this).attr('data-taccount',t_account);
            $(this).closest('tr').find('td.default').text(t_account).attr('style','color:blue;display:none;');

            if(t_account == 'DEBIT'){
                $(this).closest('tr').find('td:eq(4)').attr('style','color:blue;');
                $(this).closest('tr').find('td:eq(5)').removeClass('pointer');
            }

            if(t_account == 'CREDIT'){
                $(this).closest('tr').find('td:eq(4)').removeClass('pointer');
                $(this).closest('tr').find('td:eq(5)').attr('style','color:blue;');
            }

            return value;

        },{
         loadurl  : '<?php echo site_url('procurement/purchase_request/get_account_title'); ?>',                   
         placeholder : 'Select Account title',
         type        : "select2",
         submit      : 'ok',
         callback    : function(){
            var id = $(this).attr('data-account_id');

            $div = '<select class="subsidiary_select">'
                +'<option value=""></option>'
                +'</select>';
            $(this).parent().children('.editable_subsidiary').html($div);

            var select_subs = $(this).parent().find('.subsidiary_select');

            $.getJSON('<?php echo site_url('procurement/purchase_request/get_subsidiary');?>',{id:id},function(json){
                
                $(this).parent().children('.subsidiary_select option:gt(0)').remove();
                $(this).parent().children('.subsidiary_select option:first').text('');
                if(json.length > 0){
                    for(var i = 0; i < json.length; i++){
                        if(json[i]['account_id'] == id){
                            $('<option/>').attr('value',json[i]['id']).text(json[i]['description']).appendTo(select_subs);
                        }
                    }
                }
            });
         }
    });

    $('body').on('change','.debit',function(){
        var debit = $(this).closest('tr').find('.debit').val();

        get_total();    
    });

    $('body').on('change','.credit',function(){
        var credit = $(this).closest('tr').find('.credit').val();

        get_total();      
    });

    //for editable end

    var get_total = function(){

        var total_debit  = 0;
        var total_credit = 0;
        $('.bill_journal_contents tr').each(function(i,val){
                var debit  = remove_comma($(val).closest('tr').find('.debit').val());
                var credit = remove_comma($(val).closest('tr').find('.credit').val());
                total_debit  = +total_debit + +debit;
                total_credit = +total_credit + +credit;
        });

        $('.total_debit').html(comma(round2Fixed(total_debit)));
        $('.total_credit').html(comma(round2Fixed(total_credit)));

        if(parseFloat($('.total_debit').val()) != parseFloat($('.total_credit').val())){
            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:red;');
            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:red;');
        }else if(parseFloat($('.total_debit').val()) == parseFloat($('.total_credit').val())){
            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:blue;');
            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:blue');
        }

    }
}
</script>