<div class="content-page">
	<div class="content">
<div class="header">
	<h2>New RIS</h2>
</div>

<div class="container">
	
	<div class="row">
		<div class="col-md-1">
			<div class="content-title" style="display:none;">
				<h3>Location</h3>
			</div>
			<div class="panel panel-default" style="display:none;">		
			  <div class="panel-body">

			  		<div class="form-group" style="display:none">
			  			<div class="control-label">Company Name</div>
			  			<select name="" id="create_project" class="form-control input-sm"></select>
			  		</div>

			  		<div class="form-group">
			  			<div class="control-label">Project </div>
			  			<select name="" id="create_profit_center" class="select2" style="width:100%;" disabled="disabled"></select>					  			
		  			</div>

			  		<input type="hidden" id="account_code">			
			  		<input type='text' name="" id="to" class="form-control input-sm" style="display:none">
			  		<input type="hidden" id="projectid" value="<?php echo $this->session->userdata('Proj_Code')?>">
					
			  </div>	 
			</div>

		</div>
		<div class="col-md-11">
			<div class="content-title">
				<h3>Item Information</h3>
			</div>
			<div class="panel panel-default">		
			  <div class="panel-body">					
			  		<div class="row">
			  			<div class="col-md-4">
				  			<div class="form-group">
					  			<div class="control-label">Item Name</div>
					  			<input type="text" name="" id="item_name" style="width:100%" placeholder="Search item Description">
				  			</div>

			  			</div>
			  			<div class="col-md-4">
			  				<div class="form-group">
					  			<div class="control-label">Account Name (DEBIT)</div>
					  			 <input type="text" name="" id="accountname" style="width:100%" placeholder="Account Name">
					  		</div>
			  			</div>
			  			<div class="col-md-4">
			  				<div class="form-group">
			  					<div class="control-label">Subsidiary (DEBIT)</div>
			  					<select name="" id="subsid" class="select2" style="width:50%">
			  						<option value="selected"></option>
			  					</select>
			  				</div>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-4">
			  				<div class="form-group">
					  			<div class="control-label">Account Name (CREDIT)</div>
					  			<input type="text" name="" id="account_name" style="width:100%" placeholder="Account Name" disabled="disabled" data-desc="" data-code="">
					  		</div>
			  			</div>
			  			
			  		</div>
			  		<div class="row">	
			  			<div class="col-md-2">
			  				<div class="form-group">
					  			<div class="control-label">Quantity</div>
					  			<input type="number" name="" id="quantity" class="form-control input-sm numbers_only">
				  			</div>
			  			</div>

			  			<div class="col-md-2">
				  			<div class="form-group">
					  			<div class="control-label">Unit Cost</div>
					  			<input type="number" name="" id="unit_cost" class="form-control input-sm">
				  			</div>
			  			</div>	
			  			
			  			<div class="col-md-3">
				  			<div class="form-group">
					  			<div class="control-label">Remarks</div>
					  			<input type="text" name="" id="remarks" class="form-control input-sm">
				  			</div>
			  			</div>

			  			<div class="col-md-1">
			  				<div class="form-group">
					  			<div class="control-label">Date</div>
					  			<input type="text" name="" id="date" class="form-control input-sm date" readonly>
				  			</div>
			  			</div>
			  			
			  			<div class="col-md-2">
			  				<button id="add" class="btn btn-primary nxt-btn">Add</button>
			  			</div>

			  		</div>
			  </div>	 
			</div>

		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
								
				<div class="panel panel-default">
				<div class="table-responsive">
				  <table id="item_add" class="table table-striped">
				  	<thead>
				  		<tr>
				  			<th>Item Description</th>
				  			<th>Unit Of Measure</th>
				  			<th>Subsidiary ID</th>
				  			<th>Acct</th>
				  			<th>Qty</th>
				  			<th>Unit Cost</th>
				  			<th>Total Cost</th>
				  			<th>Remarks</th>
				  		</tr>
				  	</thead>
				  	<tbody>
				  		<tr>
				  			<td colspan='8'>No Data</td>				  		
				  		</tr>
				  	</tbody>
				  	<tfoot>
				  		<tr>
				  			<td><span id='cnt-items'></span> item(s)</td>
				  			<td></td>
				  			<td></td>
				  			<td></td>
				  			<td></td>
				  			<td><span id="total_amount"></span></td>
				  			<td></td>
				  			<td></td>
				  		</tr>
				  	</tfoot>
				  </table>
				</div>
				</div>

		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">		
			  <div class="form-footer">			  	
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
					  			<div class="control-label">Purpose</div>
					  			<select name="" id="purpose" class="select2" style="width:100%;">
					  				<option value="0"> - </option>
					  				<?php
					  					if(!empty($purpose)){
					  						foreach($purpose as $row){
					  				?>
					  				<option value="<?php echo $row['id'];?>"><?php echo $row['purpose_name'];?></option>
					  				<?php
					  						}
					  					}
					  				?>
					  			</select>
					  		</div>

							<div class="form-group">
					  			<textarea name="" id="rr_remarks" cols="30" rows="5" class="form-control input-sm" placeholder="Support Details"></textarea>					  			
					  		</div>

					  		<?php echo $this->load->view('procurement/ris/journal_entries_view');?>
						</div>
						<div class="col-md-6">
							<div class="form-group">
					  			<div class="control-label">Issued by</div>
					  			<select name="prepared_by" id="prepared_by" class="form-control input-sm"></select>
					  		</div>
							<div class="form-group">
					  			<div class="control-label">Request by</div>
					  			<select name="recommended_by" id="recommended_by" class="form-control input-sm"></select>
					  		</div>
					  		<div class="form-group">
					  			<div class="control-label">Checked by</div>
					  			<select name="checked_by" id="checked_by" class="form-control input-sm"></select>
					  		</div>
					  		<div class="form-group">
					  			<div class="control-label">Approved by</div>
					  			<select name="approved_by" id="approved_by" class="form-control input-sm"></select>
					  		</div>
					  		<div class="form-group">
					  			<div class="control-label">Received by</div>
					  			<select name="received_by" id="received_by" class="form-control input-sm"></select>
					  		</div>
					  		
							<input id="save" class="btn btn-success  col-xs-5 pull-left" type="submit" value="Save">
						</div>
					</div>					
			  </div>
			</div>
		</div>
	</div>

</div>

</div>
</div>

<script type="text/javascript">	
	var check_item = [];
	var	xhr = '' ;
	var DATA = [];
	var app = {
		init:function(){

			$('.select2').select2();
			$('.date').date();
			
			var option = {
				profit_center : $('#create_profit_center'),
				main_office : true,
			}			

			$('#create_project').get_projects(option);
			
			this.bindEvents();
			this.get_supplier();
			this.get_item();
			this.get_account_name();
			// this.get_account();

			var profit_center_value = '';
			if(typeof ($('#create_profit_center option:selected').val()) != 'undefined'){
				 profit_center_value = $('#create_profit_center option:selected').val();
			}
			
			$.signatory({
				type          : 'ris',
				prepared_by   : 'sesssion',
				recommended_by: ["4", "5", "1",profit_center_value],
				checked_by    : ["4", "5", "1",profit_center_value],
				approved_by   : ["4", "4", "1",profit_center_value],
				approved_by   : ["4", "4", "1",profit_center_value],
				received_by	  : ["4", "4", "1",profit_center_value],	
				issued_by	  : ["4", "5", "1",profit_center_value]
			});

			$('body').on('click','.remove',function(){
		        var debit = $(this).closest('tr').find('.debit').val();
		        var credit = $(this).closest('tr').find('.credit').val();
		        var total_debit = $('.total_debit').text();
		        var total_credit = $('.total_credit').text();

		        $(this).closest('tr').get(0).remove();
		        debit = debit.replace(',','');
		        credit = credit.replace(',','');
		        total_debit = total_debit.replace(',','');
		        total_credit = total_credit.replace(',','');

		        var totaldebit = 0;
		        var totalcredit = 0;

		        if(parseFloat(debit) > 0 && parseFloat(credit) == 0){
		            totaldebit = parseFloat(total_debit) - parseFloat(debit);

		            $('.total_debit').text(comma(round2Fixed(totaldebit)));
		        }else if(parseFloat(credit) > 0 && parseFloat(debit) == 0){
		            totalcredit = parseFloat(total_credit) - parseFloat(credit);

		            $('.total_credit').text(comma(round2Fixed(totalcredit)));
		        }

		        if(parseFloat($('.total_debit').text()) != parseFloat($('.total_credit').text())){
		            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:red;');
		            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:red;');
		        }

		        if(parseFloat($('.total_debit').text()) == parseFloat($('.total_credit').text())){
		            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:blue;');
		            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:blue');
		        }

		    });

			/*$('#purpose').on('change',function(){
				var purpose = $('#purpose option:selected').text();

				if(purpose == 'NEW INSTALLATION'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'INSTALLATION'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'PAID MATERIAL'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'PAID_MATERIAL'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'OFFICE SUPPLIES'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'OFFICE_SUPPLIES'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'CHANGE METER'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'CHANGE_METER'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'REPAIR LEAK'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'REPAIR_LEAK'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'REPAIR MAINTENANCE LAND TRANSPORT'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'REPAIR_MAINTENANCE'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'FOR CHLORINATION'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'FOR_CHLORINATION'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'TRANSFER OF METER'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'TRANSFER_METER'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}else if(purpose == 'PROJECT'){
					$.getJSON('<?php echo site_url('procurement/purchase_request/get_transaction_entries')?>',{type:'PROJECT'},function(json){
	                    for(var i = 0; i < json.length;i++){
	                        $div =  '<tr>' 
	                                +'<td><span class="close padding-right pull-left remove">&times;</span></td>'
	                                +'<td class="editable_code pointer">'+json[i]['account_code']+'</td>'
	                                +'<td class="editable_journal pointer" data-account_id="'+json[i]['account_id']+'" data-ledger="" data-code="'+json[i]['account_code']+'" data-taccount="'+json[i]['default']+'">'+json[i]['account_name']+'</td>'
	                                +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                                +'<select class="subsidiary_select">'
	                                +'<option value=""></option>'
	                                +'</select>'
	                                +'</td>'
	                                +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
	                                +'<td style="display:none;">MATSUP INVENTORY</td>'
	                                +'<td class="default" style="display:none;">'+json[i]['default']+'</td>'
	                                +'</tr>';               
	                        $('.bill_journal_contents').append($div);
	                    }

	                    journal_editable();
	                });
				}
			});*/
			
		},bindEvents:function(){

			$('#add').on('click',this.add);
			$('#item_add').on('click','.remove',this.remove_data);
			$('#save').on('click',this.save);
			$('#create_profit_center').on('change',this.location);

		},location:function(){
			
			$('#location').val($('#create_profit_center option:selected').data('location'));
			$('#to').val($('#create_profit_center option:selected').data('to'));
		},get_item:function(){
			$("#item_name").select2({
		     	 	placeholder: "Search Items Here",
		     	 	allowClear: true,
				    ajax: {
				        url: '<?php echo site_url("procurement/purchase_request/get_items"); ?>',
				        dataType: 'json',
				        type: "GET",
				        quietMillis: 50,
				        data: function (term) {
				            return {
				                q: term
				            };
				        },				     
				        results: function (data){
				            return {
				                results: $.map(data, function (item) {					                              
				                    return {
				                        text: item.description,
				                        id  : item.group_detail_id,
				                        group_id : item.group_id,
				                        account_id : item.account_id,
				                        account_code : item.accnt_code,
				                        account_name : item.accnt_description,
				                        account_default : item.accnt_default,
				                        unit_cost : item.bal_unit_cost,				                        
				                        unit      : item.unit_measure,
				                        desc : item.item_description,
				                        subsidiary_id : item.subsidiary_id,
				                        subsidiary_name : item.subsidiary_name
				                    }
				                })
				            };
				        }
				    },initSelection: function (element, callback){
				         	var id = $(element).val();
				         	$(element).val();
					},formatSelection:function(object,container){
						var data = {
		                    unit_cost : object.unit_cost,
		                    item_desc : object.desc,
		                    item_id :object.id,
		                    account_id : object.account_id,
		                    accnt_name : object.account_name,
		                    accnt_code : object.account_code,
		                    accnt_default : object.account_default,
		                    subsidiary_id : object.subsidiary_id,
		                    subsidiary_name : object.subsidiary_name
		                };
		                var cost = parseFloat(data.unit_cost).toFixed(2);
		                $('#unit_cost').val(cost);
		                $('#account_name').val(data.accnt_name);
		                $('#account_name').attr('data-code', data.accnt_code);
		                $('#account_name').attr('data-id', data.account_id);
		                $('#account_name').attr('data-default',data.accnt_default);
		                $('#account_name').attr('data-subsidiaryid',data.subsidiary_id);
		                $('#account_name').attr('data-subsidiaryname',data.subsidiary_name);
		                return object.text;
					}
			});

		},get_account_name:function(){
			$("#accountname").select2({
		     	 	placeholder: "Search Account Here",
		     	 	allowClear: true,
				    ajax: {
				        url: '<?php echo site_url("procurement/purchase_request/get_account_account"); ?>',
				        dataType: 'json',
				        type: "GET",
				        quietMillis: 50,
				        data: function (term) {
				            return {
				                q: term
				            };
				        },				     
				        results: function (data){
				            return {
				                results: $.map(data, function (item) {					                              
				                    return {
				                        text: item.account_description,
				                        id  : item.id,
				                        account_id : item.type_id,
				                        account_code : item.account_code,
				                        account_name : item.account_description,
				                        desc : item.account_description,
				                        account_default : item.t_account
				                    }
				                })
				            };
				        }
				    },initSelection: function (element, callback){
				         	var id = $(element).val();
				         	$(element).val();
					},formatSelection:function(object,container){
						var data = {
		                    item_desc : object.desc,
		                    item_id :object.id,
		                    account_id : object.account_id,
		                    accnt_name : object.account_name,
		                    accnt_code : object.account_code,
		                    accnt_default : object.account_default
		                };
		                $('#accountname').val(data.accnt_name);
		                $('#accountname').attr('data-code', data.accnt_code);
		                $('#accountname').attr('data-id', data.account_id);
		                $('#accountname').attr('data-desc', data.item_desc);
		                $('#accoutnname').attr('data-default', data.accnt_default);

		                $get = {
		                	account_id: data.item_id
		                };
		                 $.getJSON('<?php echo site_url('procurement/purchase_request/get_account_subsidiary')?>',$get,function(json){
		                	$('#subsid option:gt(0)').remove();
		                	$('#subsid option:first').val('0');
		                	$('#subsid option:first').text('Select Item');
		                	for(var i = 0; i < json.length; i++){
			                	$('<option/>').attr('value',json[i]['id']).attr('data-accountid',json[i]['account_id']).attr('data-desc',json[i]['description']).attr('data-id', json[i]['id']).text(json[i]['description']).appendTo($('#subsid'));
			                }
		                });

		                return object.text;
					}
			});

		},get_supplier:function(){
			$("#supplier").select2({
		     	 	placeholder: "Select Supplier",
		     	 	allowClear: true,
				    ajax: {
				        url: '<?php echo site_url("setup/supplier_setup/get_supplier"); ?>',
				        dataType: 'json',
				        type: "GET",
				        quietMillis: 50,
				        data: function (term) {
				            return {
				                q: term
				            };
				        },				     
				        results: function (data){
				            return {
				                results: $.map(data, function (item) {					                              
				                    return {
				                        text: item.business_name,
				                        id  : item.business_number,
				                        desc : item.business_name
				                    }
				                })
				            };
				        }
				    },initSelection: function (element, callback){
				         	var id = $(element).val();
					}
			});
						
		},get_category:function(){

			$('#dp-unit').html($('#item_name option:selected').attr('data-unit'));
			$post = {
				group_id : $('#item_name option:selected').val(),
			}
			$.post('<?php echo base_url().index_page();?>/procurement/purchase_request/get_category',$post,function(response){
				 $('#item_category').select({
				 	json : response,
				 	attr : {
				 		   text  : 'group_description',
				 		   value : 'group_id',
				 	}
				 });
				var account_id = $('#item_name option:selected').attr('data-account_id');
				$('#account_description').find('option').each(function(i,value){
					if($(value).data('account_id') == account_id){
						$(value).attr({'selected':'selected'});
						$('#account_description').trigger('change');
					}else{
						$(value).removeAttr('selected');
					}
				});

			},'json');

			app.get_incomeAcct();

		},add:function(){

				var unit_cost  = $('#unit_cost').val();
				var qty = $('#quantity').val();
				var amount = $('#amount').val();
				var item = $("#item_name").select2('data');
				var account_code = $('#account_name').attr('data-code');
				var account_id = $('#account_name').attr('data-id');
				var account_name = $('#account_name').val();
				var account_default = $('#account_name').attr('data-default');
				var accountcode = $('#accountname').attr('data-code');
				var accountid = $('#accountname').attr('data-id');
				var accountname = $('#accountname').attr('data-desc');
				var accountdefault = $('#accountname').attr('data-default');
				var subsidiaryid = $('#account_name').attr('data-subsidiaryid');
				var subsidiaryname = $('#account_name').attr('data-subsidiaryname');
				var sub_id = $('#subsid option:selected').attr('data-id');
				var sub_name = $('#subsid option:selected').attr('data-desc');

				// alert($('#subsid').val());
				// alert(accountcode);
				// $post={
				// 	item_id : item.id
				// }
				// $.post('<?php echo base_url().index_page();?>/procurement/purchase_request/get_category?>',$post,function(response){
				// 	switch(response){
				// 		case "success":						   	
				// 			$.save({action : 'success',reload : 'true'});
				// 			window.location = '<?php echo base_url().index_page(); ?>/transaction_list/ris'
				// 		break;

				// 		default:
				// 		    alert(response);
				// 			$.save({action : 'error',reload : 'false'});
				// 		break;
				// 	}
				// });
				if(qty ==''){
						alert('No Quantity');
					return false;					
				}else if(item == null){
						alert('No Item Selected');
					return false;
				}
				if($('#subsid option:selected').val() == '0'){
					sub_id = '';
					sub_name = '';
				}

				if(accountid == '0' || account_id == 0){
					alert('No Account Name(DEBIT) selected!');
					return false;
				}
				
				unit_cost = unit_cost.replace(',','');
				amount = parseFloat(qty) * parseFloat(unit_cost);

				// alert(sub_name);
				var data = {
					'item_id'             : item.id,
					'item_description'    : item.desc,
					'unit_measure'        : item.unit,
					'quantity'            : $('#quantity').val(), 
					'unit_cost'           : comma(parseFloat(unit_cost).toFixed(2)),
					'amount'              : comma(parseFloat(amount).toFixed(2)),
					'remarks'             : $('#remarks').val(),
					'account_id'		  : account_id,
					'account_code'		  : account_code,
					'account_name'        : account_name,
					'accountid'			  : accountid,
					'accountname' 		  : accountname,
					'accountcode'  		  : accountcode,
					'sub_id'			  : sub_id,
					'subsidiary_name'	  : sub_name
				};

				if(jQuery.inArray(data.item_description, check_item) !== -1){
					alert('Item Already in the list');				
					return false;
				}
				//add journal entries
				if(accountid != 0 || accountid != '0'){
					$div =  '<tr>' 
                            +'<td><span class="close padding-right pull-left remove" data-itemid="'+data.item_id+'">&times;</span></td>'
                            +'<td class="editable_code pointer">'+accountcode+'</td>'
                            +'<td class="editable_journal pointer" data-account_id="'+accountid+'" data-ledger="" data-code="'+accountcode+'" data-taccount="'+accountdefault+'">'+accountname+'</td>'
                            +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
                            +'<select class="subsidiary_select" style="width:100px;">'
                            +'<option value="'+sub_id+'">'+sub_name+'</option>'
                            +'</select>'
                            +'</td>'
                            +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" style="width:50px;" value="'+comma(round2Fixed(amount))+'" /></td>'
                            +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
                            +'<td style="display:none;">MATSUP INVENTORY</td>'
                            +'<td class="default" style="display:none;">'+accountdefault+'</td>'
                            +'</tr>';               
                    $('.bill_journal_contents').append($div);
				}

				if(account_id != 0 || account_id != '0'){
					$div =  '<tr>' 
                            +'<td><span class="close padding-right pull-left remove" data-itemid="'+data.item_id+'">&times;</span></td>'
                            +'<td class="editable_code pointer">'+account_code+'</td>'
                            +'<td class="editable_journal pointer" data-account_id="'+account_id+'" data-ledger="" data-code="'+account_code+'" data-taccount="'+account_default+'">'+account_name+'</td>'
                            +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
                            +'<select class="subsidiary_select" style="width:100px;">'
                            +'<option value="'+subsidiaryid+'">'+subsidiaryname+'</option>'
                            +'</select>'
                            +'</td>'
                            +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" value="0.00" style="width:50px;" /></td>'
                            +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" style="width:50px;" value="'+comma(round2Fixed(amount))+'" /></td>'
                            +'<td style="display:none;">MATSUP INVENTORY</td>'
                            +'<td class="default" style="display:none;">'+account_default+'</td>'
                            +'</tr>';               
                    $('.bill_journal_contents').append($div);
				}

				journal_editable();
				get_total();
				
				check_item.push(data.item_id);

				DATA.push(data);
				
				$('#amount').val('');
				$('#remarks').val('');
				$('#item_name').select2('val','');
				$('#quantity').val('');
				$('#unit_cost').val('');
				$('#account_name').val('');
				$('#accountname').val('');
				$('body').on('#subsid','.click', function(){
					$(this).parent().children('#subsid option:gt(0)').remove();
					$(this).parent().children('#subsid option:first').text('');
				});
				$('#subsid').val('0').trigger("change");

				app.render();

		},render:function(){
			if(DATA.length>0){
				$('#item_add tbody').html('');

				$.each(DATA,function(i,value){
									var content  ="<tr>";
										content +="<td>"+value.item_description+"</td>";
										content +="<td>"+value.unit_measure+"</td>";
										content +="<td>"+value.subsidiary_name+"</td>";
										content +="<td data-id="+value.accountid+" data-desc="+value.accountname+">"+value.accountcode+"</td>";
										content +="<td>"+value.quantity+"</td>";
										content +="<td>"+value.unit_cost+"</td>";
										content +="<td>"+value.amount+"</td>";
										content +="<td>"+value.remarks+"</td>";
										content +="<td><a href='javascript:void(0)' class='remove' data-id='"+i+"' data-itemid='"+value.item_id+"' data-amount='"+value.amount+"'>Remove</a></td>";
										content +="</tr>";
					$('#item_add tbody').append(content);
				})
				$('#cnt-items').html(DATA.length);

				/*if($(DATA).toArray().length == 1){
					var amount = $('#amount').val();
					$('#total_amount').html(amount);
				}else if($(DATA).toArray().length >= 2){
					var temp = $('#total_amount').html();
					var amount = $('#amount').val();
					var total_amount = 0;
					total_amount = parseFloat(temp) + parseFloat(amount);
					$('#total_amount').html(total_amount);
				}*/

			}else{
				$('#item_add tbody').html('<tr><td colspan="8">No Data</td></tr>');
				$('#cnt-items').html('');
			}
		},remove_data:function(){
					var bool = confirm('Are you Sure?');
					if(bool){
						var id = $(this).data('id');
						var amount = $(this).data('amount');
						var total = $('#total_amount').html();
						var item_id = $(this).attr('data-itemid');
						
						DATA.splice(id,1);
						check_item.splice(id,1);

						$('.bill_journal_contents tr').each(function(i,val){
							var itemid = $(val).closest('tr').find('.remove').attr('data-itemid');

							if(item_id == itemid){
								var debit = $(val).closest('tr').find('.debit').val();
						        var credit = $(val).closest('tr').find('.credit').val();
						        var total_debit = $('.total_debit').text();
						        var total_credit = $('.total_credit').text();

						        $(val).closest('tr').get(0).remove();
						        debit = debit.replace(',','');
						        credit = credit.replace(',','');
						        total_debit = total_debit.replace(',','');
						        total_credit = total_credit.replace(',','');

						        var totaldebit = 0;
						        var totalcredit = 0;

						        if(parseFloat(debit) > 0 && parseFloat(credit) == 0){
						            totaldebit = parseFloat(total_debit) - parseFloat(debit);

						            $('.total_debit').text(comma(round2Fixed(totaldebit)));
						        }else if(parseFloat(credit) > 0 && parseFloat(debit) == 0){
						            totalcredit = parseFloat(total_credit) - parseFloat(credit);

						            $('.total_credit').text(comma(round2Fixed(totalcredit)));
						        }

						        if(parseFloat($('.total_debit').text()) != parseFloat($('.total_credit').text())){
						            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:red;');
						            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:red;');
						        }

						        if(parseFloat($('.total_debit').text()) == parseFloat($('.total_credit').text())){
						            $('.total_debit').attr('style','font-weight:bold;font-size:15px;color:blue;');
						            $('.total_credit').attr('style','font-weight:bold;font-size:15px;color:blue');
						        }

							}
						});  

						/*total = parseFloat(total) - parseFloat(amount);

						$('#total_amount').html(total);*/

						app.render();
					}
		},save:function(){

				if($('.required').required()){
					alert('Required Fields');
					return false;
				}

				var check_journal = false;

		        $('.bill_journal_contents tr').each(function(i,val){             
		            if($(val).find('.editable_journal').attr('data-account_id') > 0){
		                check_journal = false;
		            }else{
		                check_journal = true;
		            }
		        });

		        if(check_journal){
		            alert('Please select account Title');
		            return false;
		        }

		        var total_debit  = $('.total_debit').text();
		        var total_credit = $('.total_credit').text();

		        var check_drcr = false;

		        total_debit = total_debit.replace(',','');
		        total_credit = total_credit.replace(',','');

		        if(parseFloat(total_debit) == parseFloat(total_credit)){
		            check_drcr = true;
		        }else{
		            check_drcr = false;
		        }

		        if(check_drcr == false){
		            alert('Total Debit Amount and Total Credit Amount are not equal!');
		            return false;
		        }

		        var journal_details = new Array();

		        $('.bill_journal_contents tr').each(function(i,val){
		            var obj = {
		                account_code : $(val).closest('tr').find('td.editable_code').text(),
		                account_id : $(val).closest('tr').find('td.editable_journal').attr('data-account_id'),
		                account_title : $(val).closest('tr').find('td.editable_journal').text(),
		                subsidiary_id : $(val).closest('tr').find('.subsidiary_select option:selected').val(),
		                subsidiary : $(val).closest('tr').find('.subsidiary_select option:selected').text(),
		                debit_amount : $(val).closest('tr').find('.debit').val(),
		                credit_amount : $(val).closest('tr').find('.credit').val(),
		                type : $(val).closest('tr').find('td:eq(6)').text(),
		                def : $(val).closest('tr').find('td.editable_journal').attr('data-taccount')
		            }
		            journal_details.push(obj);
		        });


				if(DATA.length <= 0){
					alert('No Items Added');
					return false;
				}
				
				if($('#approved_by option:selected').val() == ''){
					alert('Please select approved by signatory');
					return false;
				}

				if($('#prepared_by option:selected').val() == ''){
					alert('Select Prepared by User');
					return false;
				}
				
				var bool = confirm('Are you sure to Proceed?');
				if(!bool){
					return false;
				}
				
				var	$post = {
					'Date'           : $('#date').val(),
					'project_id'     : $('#projectid').val(),
					'title_id'       : $('#create_project option:selected').val(),
					'data'           : DATA,
					'remarks'        : $('#rr_remarks').val(),
					'recommended_by' : $('#recommended_by option:selected').val(),
					'prepared_by'    : $('#prepared_by option:selected').val(),
					'checked_by'     : $('#checked_by option:selected').val(),
					'approved_by'    : $('#approved_by option:selected').val(),
					'issued_by'      : $('#issued_by option:selected').val(),
					'received_by'	 : $('#received_by option:selected').val(),
					'journal_details': JSON.stringify(journal_details),
					'purpose'		 : $('#purpose option:selected').text()
				};
				// alert($('#recommended_by option:selected').val());
				// alert($('#checked_by option:selected').val());
				// alert($('#approved_by option:selected').val());
				// alert($('#received_by option:selected').val());
				// alert($('#prepared_by option:selected').val());
				// return false;
				$.save({appendTo : 'body'});

		        if(xhr && xhr.readystate != 4){
		            xhr.abort();
		        }

				xhr = $.post('<?php echo base_url().index_page(); ?>/procurement/purchase_request/save_ris',$post,function(response){					
					switch(response){

						case "success":						   	
							$.save({action : 'success',reload : 'true'});
							window.location = '<?php echo base_url().index_page(); ?>/transaction_list/ris'
						break;

						default:
						    alert(response);
							$.save({action : 'error',reload : 'false'});
						break;

					}
				},'json').error(function(){
					$.save({action : 'error'});
				});
		}
	}

$(function(){
	app.init();
});

function journal_editable(){
    //for editable start
    $('.editable_journal').editable(function(value,settings,data){                  
            var id = $(data).find(':selected').attr('data-account_id');
            $(this).attr('data-account_id',id);

            var ledger = $(data).find(':selected').attr('data-ledger');
            $(this).attr('data-ledger',ledger);

            var code = $(data).find(':selected').attr('data-code');
            $(this).attr('data-code',code);

            $(this).closest('tr').find('td.editable_code').text(code);

            var t_account = $(data).find(':selected').attr('data-taccount');
            $(this).attr('data-taccount',t_account);
            $(this).closest('tr').find('td.default').text(t_account).attr('style','color:blue;display:none;');

            if(t_account == 'DEBIT'){
                $(this).closest('tr').find('td:eq(4)').attr('style','color:blue;');
                $(this).closest('tr').find('td:eq(5)').removeClass('pointer');
            }

            if(t_account == 'CREDIT'){
                $(this).closest('tr').find('td:eq(4)').removeClass('pointer');
                $(this).closest('tr').find('td:eq(5)').attr('style','color:blue;');
            }

            return value;

        },{
         loadurl  : '<?php echo site_url('procurement/purchase_request/get_account_title'); ?>',                   
         placeholder : 'Select Account title',
         type        : "select2",
         submit      : 'ok',
         callback    : function(){
            var id = $(this).attr('data-account_id');

            $div = '<select class="subsidiary_select">'
                +'<option value=""></option>'
                +'</select>';
            $(this).parent().children('.editable_subsidiary').html($div);

            var select_subs = $(this).parent().find('.subsidiary_select');

            $.getJSON('<?php echo site_url('procurement/purchase_request/get_subsidiary');?>',{id:id},function(json){
                
                $(this).parent().children('.subsidiary_select option:gt(0)').remove();
                $(this).parent().children('.subsidiary_select option:first').text('');
                if(json.length > 0){
                    for(var i = 0; i < json.length; i++){
                        if(json[i]['account_id'] == id){
                            $('<option/>').attr('value',json[i]['id']).text(json[i]['description']).appendTo(select_subs);
                        }
                    }
                }
            });
         }
    });

    $('body').on('change','.debit',function(){
        var debit = $(this).closest('tr').find('.debit').val();

        get_total();    
    });

    $('body').on('change','.credit',function(){
        var credit = $(this).closest('tr').find('.credit').val();

        get_total();      
    });

    //for editable end
}

var get_total = function(){

    var total_debit  = 0;
    var total_credit = 0;
    $('.bill_journal_contents tr').each(function(i,val){
            var debit  = remove_comma($(val).closest('tr').find('.debit').val());
            var credit = remove_comma($(val).closest('tr').find('.credit').val());
            total_debit  = +total_debit + +debit;
            total_credit = +total_credit + +credit;
    });

    $('.total_debit').html(comma(round2Fixed(total_debit)));
    $('.total_credit').html(comma(round2Fixed(total_credit)));

}
</script>