<input type="hidden" value="<?php echo $main_data['ris_id'] ?>" id="transaction_id">
 
						<div class="t-content">
							<div class="form-group" style="display:none">
					  			<div class="control-label">Company Name</div>
					  			<select name="" id="create_project" class="form-control input-sm"></select>
					  		</div>

					  		<div class="form-group" style="display:none">
					  			<div class="control-label">Project </div>
					  			<select name="" id="create_profit_center" class="select2" style="width:100%;" disabled="disabled"></select>					  			
				  			</div>

					  		<input type="hidden" id="account_code">			
					  		<input type='text' name="" id="to" class="form-control input-sm" style="display:none">
					  		<input type="hidden" id="projectid" value="<?php echo $this->session->userdata('Proj_Code')?>">							
							<div class="t-header">
								<h4><?php echo $main_data['risNo']; ?></h4>
							</div>
							
							<div class="row">
								<!-- <div class="col-xs-5"> -->
									<!-- <div class="control-group">									 -->
										<!-- <?php if(strtoupper($main_data['ris_status'])=="ACTIVE"): ?>
										<a href="<?php echo base_url().index_page();?>/print/ris/<?php echo $main_data['risNo']; ?>" target="_blank" class="action-status"><i class="fa fa-print"></i> Print</a>
										<a class="action-status history-link" data-method="ris" data-type="edit" data-value="<?php echo $main_data['risNo']; ?>" href="<?php echo base_url().index_page();?>/transaction_list/ris/<?php echo $main_data['risNo']; ?>/edit" target="_blank" class="action-status"><i class="fa fa-pencil"></i> Edit</a>
										<?php else: ?>
										<a href="<?php echo base_url().index_page();?>/print/ris_gate_pass/<?php echo $main_data['risNo']; ?>" target="_blank" class="action-status"><i class="fa fa-print"></i> Print Gate Pass</a>
											<?php
												if($this->lib_auth->restriction('USER'))
												{
													
													if(strtoupper($main_data['ris_status'])!='APPROVED'){
														echo $this->lib_transaction->status2($main_data['ris_status'],$main_data['risNo']);
													}else{
														echo "<span class='label label-success'>".$main_data['ris_status']."</span>";
													}
													
												}										
											?>
										<?php endif; ?> -->
									<!-- </div> -->
								<!-- </div> -->
								
								<div class="col-xs-5">
									
									<!-- <?php 						
									
										$branch_type = $this->session->userdata('branch_type');										
										$where = '';									
										switch($branch_type){
											case "MAIN OFFICE":
																								

													if($this->lib_auth->restriction('USER'))
													{
														echo $this->lib_transaction->ris_status($main_data['ris_status'],$main_data['ris_id']);
													}

													/*if($this->lib_auth->restriction('CANVASS USER'))
													{
														if(strtoupper($main_data['status'])=='APPROVED'){
															echo $this->extra->label($main_data['status']);	
														}	
													}*/
														
													

											break;
											case "PROFIT CENTER":
												echo $this->extra->label($main_data['ris_status']);									
											break;
											default:
													
											break;										
										}
																
								  ?> -->
								  <?php 
									$branch_type = $this->session->userdata('branch_type');
									$where = ''; 
									switch($branch_type){
										case "MAIN OFFICE":														
														?>
														<div class="row">
															<div class="col-md-6">	
																<div class="control-group">															
																<?php if($this->lib_auth->restriction('PO USER')): ?>
																 	
																	<!-- <span class="control-item-group">
																	<?php if($main_data['status'] !='CANCELLED'): ?>
																		<a class="cancel-event action-status" href="javascript:void(0)">Cancel</a>
																	<?php else: ?>
																		<span class="label label-danger">CANCELLED</span>	
																	<?php endif; ?>
																	</span> -->

																	<span class="control-item-group">
																		<a class="action-status history-back" href="<?php echo base_url().index_page();?>/transaction_list/ris/<?php echo $main_data['risNo']; ?>"> < Back</a>
																	</span>
																 </div>
																 <?php endif; ?>
															</div>
																<div class="col-md-6">
																<?php if($this->lib_auth->restriction('RIS USER')): ?>
																<?php /*echo $this->lib_transaction->po_status($main_data['status'],$main_data['po_number']); */?>
																<?php endif; ?>
																<!-- <div class="control-item-group">
																	<a class="received-event action-status" href="javascript:void(0)">Received</a>
																</div> -->																
															</div>
														</div>
												<?php

										break;
										case "PROFIT CENTER":
										
										break;			
										default:
											
										break;
									}
								  ?>

								  
								</div>
							</div>
													
							<div class="row" style="margin-top:10px">
								<div class="col-xs-6">
									<div class="t-title">
										<div>Created From : </div> 
										<strong><?php echo $main_data['project_requestor']; ?></strong>
									</div>
									<div class="t-title">
										<div>Date: </div>
										<strong><p><?php echo $main_data['risDate'] ?></p></strong>
									</div>
									<div class="t-title">
										<div>Remarks : </div>
										<strong><p><?php echo $main_data['ris_remarks'] ?></p></strong>
									</div>
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									<input type="text" name="" class="itemname" style="width:100%" placeholder="Search item Description">
								</div>

								<div class="col-md-2">
									<button id="add" class="btn btn-primary">Add Item</button>
								</div>
							</div>
							
							<div class="table-responsive" style="overflow:auto">
							<table id="ris-item-list" class="table table-item">
								<thead>
									<tr>										
										<th></th>
										<th>Item Name</th>				
										<th>Unit</th>
										<th class="td-number">Qty</th>										
										<th class="td-number">Unit Price</th>
										<th class="td-number">Total</th>
										<th>Remarks</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$grand_total = 0;
										foreach ($details_data as $row): 
										?>
										<tr>
											<td><a href='javascript:void(0)' class='remove' data-id="<?php echo $row['itemNo'];?>" data-risid="<?php echo $main_data['ris_id'] ?>">&times;</a></td>
											<td class="item_name"><?php echo $row['item_name'];?></td>
											<td class="unit_measure"><?php echo $row['unitmeasure'];?></td>
											<td class="itemNo" style="display:none"><?php echo $row['itemNo'];?></td>
											<td class="td-qty td-number"><input type="number" style="width:80px" class="input_qty" value="<?php echo $row['quantity'];?>"></td>											
											<?php $cost = (empty($row['discounted_price']))? $row['unit_cost'] : $row['discounted_price']; ?>
											<td class="td-number"> <input type="text" style="width:100px" class="input_cost" value="<?php echo $this->extra->number_format($cost);?>"></td>
											<td class="td-number total_cost"><?php echo $total_cost = $row['unit_cost'] * $row['quantity'];?></td>
											<?php $grand_total += $total_cost;?>
											<td class="remarks"><?php echo $row['remarkz'];?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
								<tfoot>
									<tr>
										<td><?php echo count($details_data); ?> item(s)</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td class="td-number"></td>
										<td></td>
									</tr>
								</tfoot>
							</table>
							</div>							
							<div class="row">
								<div class="col-xs-6">
									<div class="t-title">
										<div>Prepared By : </div> 
										<strong><?php echo $main_data['preparedBy_name']; ?></strong>										
									</div>
								</div>
								<div class="col-xs-6">
									<div class="t-title">
										<div>Approved By : </div> 
										<?php if(!empty($main_data['approvedBy_name'])){ ?>
										<strong id="approve_exist"><?php echo $main_data['approvedBy_name']; ?></strong>
										<?php }else{ ?>
										<select name="approved_by" id="approved_by" class="form-control input-sm" style="width:350px;"></select>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="t-title">
										<div>Checked By : </div> 
										<strong><?php echo $main_data['checkedBy_name']; ?></strong>										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-primary pull-right" id="save">Apply Changes</button>
								</div>								
							</div>
						</div>



<script>
	$(function(){
		var check_item = [];
		var event_app = {
			init:function(){

				var option = {
					profit_center : $('#create_profit_center'),
					main_office : true,
				}

				$('#create_project').get_projects(option);

				var profit_center_value = '';
				if(typeof ($('#create_profit_center option:selected').val()) != 'undefined'){
					 profit_center_value = $('#create_profit_center option:selected').val();
				}

				$.signatory({
					type          : 'ris',
					approved_by   : ["4", "4", "1",profit_center_value],
				});

				this.bindEvent();
			},
			bindEvent:function(){

				$('.pending-event').on('click',this.pending_event);
				$('.approved-event').on('click',this.approved_event);
				$('.reject-event').on('click',this.reject_event);
				$('.cancel-event').on('click',this.cancel_event);
				$('#add').on('click',this.add);
				$('#save').on('click',this.save);
				$(".itemname").select2({
		     	 	placeholder: "Search Items Here",
		     	 	allowClear: true,
				    ajax: {
				        url: '<?php echo site_url("procurement/purchase_request/get_items"); ?>',
				        dataType: 'json',
				        type: "GET",
				        quietMillis: 50,
				        data: function (term) {
				            return {
				                q: term
				            };
				        },				     
				        results: function (data){
				            return {
				                results: $.map(data, function (item) {					                              
				                    return {
				                        text: item.description,
				                        id  : item.group_detail_id,
				                        group_id : item.group_id,
				                        account_id : item.account_id,
				                        unit_cost : item.unit_cost,
				                        unit      : item.unit_measure,
				                        desc : item.item_description,
				                        group_desc : item.group_description
				                    }
				                })
				            };
				        }
				    },initSelection: function (element, callback){
				         	var id = $(element).val();
					}
				});

				$('body').on('click','.remove',function(){
					var item_no = $(this).attr('data-id');
					var idx = $.inArray(item_no, check_item);
					var total = $('#grand_total').text();
					var totalcost = $(this).closest('tr').find('.total_cost').text();

					total = total.replace(',','');
					totalcost = totalcost.replace(',','');

					total = parseFloat(total) - parseFloat(totalcost);

					check_item.splice(idx,1);
					$(this).closest('tr').get(0).remove();
					$('#grand_total').text(comma(total.toFixed(2)));
				});

				
			},pending_event:function(){

				$post = {
					id     : $('#transaction_id').val(),
					transaction_no : "<?php echo $main_data['risNo']; ?>",
					status : 'PENDING',
					type   : 'ris',
					remarks: '',
					approve: $('#approved_by option:selected').val()
				};
				//event_app.execute_query($post);

			},approved_event:function(){

				if($('.required').required()){
					return false;
				}
				
				var bool = confirm('Are you sure?');
				if(!bool){
					return false;
				}
				var item_container = new Array();
				$('.approved-qty').each(function(i,val){

					var item = {
						qty    : $(val).val(),
						itemNo : $(val).attr('data-itemno'),
					}
					// alert();
					item_container.push(item);
				});
				
				var approved_by_exist= $('#approve_exist').text();
				if(approved_by_exist == ''){
					approved_by_exist = $('#approved_by option:selected').val();
				}

				var $post = {
					'id'		: $('#transaction_id').val(),
					'status'	: 'APPROVED',
					'transaction_no': "<?php echo $main_data['risNo']; ?>",
					'type'		: 'RIS',
					'remarks'	: '',
					'approve'	: approved_by_exist,
					'qty'		: $('#ris_qty').val()
				};

				event_app.execute_query($post);
				
			},reject_event:function(){
				var bool = confirm('Are you sure?');
				if(!bool){
					return false;
				}

				var remarks = prompt('Reason for Rejection');
					
				if (remarks == '') {
					alert('Please add reason');
					return false;
				}

				var approved_by_exist= $('#approve_exist').text();
				if(approved_by_exist == ''){
					approved_by_exist = $('#approved_by option:selected').val();
				}
				$post = {
					id     : $('#transaction_id').val(),					
					status : 'REJECTED',
					transaction_no : "<?php echo $main_data['risNo']; ?>",
					remarks: remarks,
					type   : 'RIS',
					approve: approved_by_exist
				};
				
				event_app.execute_reject($post);

			},cancel_event:function(){
				var bool = confirm('Are you sure?');
				if(!bool){
					return false;
				}

				$post = {
					id     : $('#transaction_id').val(),					
					status : 'CANCEL',
					transaction_no : "<?php echo $main_data['risNo']; ?>",
					remarks: '',
					type   : 'RIS',
					approve: $('#approved_by option:selected').val()
				};				
				event_app.execute_query($post);

			},execute_query:function($post){
					$.post('<?php echo base_url().index_page();?>/transaction/change_status',$post,function(response){
					switch($.trim(response)){
						case "1":						
							alert('Successfully Updated');
							/*updateContent();
							updateStatus($post.status)*/;
							
							$.fancybox.close();
							location.reload('true');
						break;
					}
					
				});
			},execute_reject:function($post){
					$.post('<?php echo base_url().index_page();?>/transaction/ris_change',$post,function(response){
						// switch($.trim(response)){
						// 	case "1":						
								alert('Successfully Updated');
								/*updateContent();
								updateStatus($post.status)*/;
								
								$.fancybox.close();
								location.reload('true');
						// 	break;
						// }
						
				});
			},save:function(){
				var bool = confirm('Are you sure?');		

				if(!bool){
					return false;
				}
				var data_cont = new Array();
				$('#ris-item-list tbody tr').each(function(i,val){
					var data = {
						item_name        : $(val).find('.item_name').text(),
						unit_measure     : $(val).find('.unit_measure').text(),
						item_no          : $(val).find('.itemNo').text(),
						quantity         : $(val).find('.input_qty').val(),
						unit_cost 		 : $(val).find('.input_cost').val().replace(/,/g,''),
						remarks          : $(val).find('.remarks').text(),
					}
					data_cont.push(data);
				});

				$post = {
					ris_id        : $('#transaction_id').val(),
					transaction_no : '<?php echo $main_data['risNo']; ?>',
					data         : data_cont,
				}

				$.post('<?php echo base_url().index_page();?>/procurement/purchase_order/edit_ris',$post,function(response){
					switch($.trim(response)){
						case "1":
							alert('Successfully Updated');
							updateContent();
							History.back();
							// $.fancybox.close();
							// location.reload('true');
						break;

						default : 
						 	alert('Internal Server Error');
						break;
					}					
				});

			},add:function(){
				var item = $(".itemname").select2('data');
				var ris_id = $('#transaction_id').val();

				var data = {
					'item_no'          : item.id,
					'item_description' : item.desc,
					'unit' 		       : item.unit,
					'group_description' : item.group_desc
				};

				if(jQuery.inArray(data.item_no, check_item) !== -1){
					alert('Item Already in the list');				
					return false;
				}
				
				check_item.push(data.item_no);

				var div = "";

				div +='<tr>';
					div +='<td><a href="javascript:void(0)" class="remove" data-id="'+data.item_no+'" data-poid="'+data.ris_id+'">&times;</a></td>';
					div +='<td class="item_name">'+data.group_description+' - '+data.item_description+'</td>';
					div +='<td class="unit_measure">'+data.unit+'</td>';
					div +='<td class="itemNo" style="display:none;">'+data.item_no+'</td>';
					div +='<td class="td-qty td-number"><input type="number" style="width:80px;" class="input_qty" value="0" /></td>';
					div +='<td class="td-number"> <input type="text" style="width:100px" class="input_cost" value="0" /></td>';
					div +='<td class="td-number total_cost"></td>';
					div +='<td class="remarks"><input type="text" style="width:200px;" class="input_remarks" value="" /></td>';
				div +='</tr>';

				$('#ris-item-list tbody').append(div);

				$('.input_qty,.input_cost').on('change',function(){

					var input_qty  = $(this).closest('tr').find('.input_qty').val();
					var input_cost = $(this).closest('tr').find('.input_cost').val().replace(/,/g,'');			
					var total      = input_qty * input_cost;
					$(this).closest('tr').find('.total_cost').html(comma(total.toFixed(2)));
					$(this).closest('tr').find('.remove').attr('data-amount',total);
					var grand_total = 0;
					$('#tbl-po tbody tr').each(function(i,val){
						grand_total += parseFloat($(val).find('.total_cost').text().replace(/,/g,''));
					});
					$('#grand_total').html(comma(grand_total.toFixed(2)));
				});

				$('.input_remarks').on('keypress',function(e){
					var code = e.keyCode || e.which;
					if(code == 13){
						var remarks = $(this).closest('tr').find('.input_remarks').val();
						$(this).closest('tr').find('.remarks').text(remarks);
					}
				});

				/*$('body').on('click','.remove',function(){
					var item_no = $(this).attr('data-id');
					var idx = $.inArray(item_no, check_item);
					var total = $('#grand_total').text();
					var totalcost = $(this).closest('tr').find('.total_cost').text();

					alert(total);

					total = total.replace(',','');
					totalcost = totalcost.replace(',','');

					total = parseFloat(total) - parseFloat(totalcost);

					check_item.splice(idx,1);
					$(this).closest('tr').get(0).remove();
					$('#grand_total').text(comma(total.toFixed(2)));
				});*/
			}

		}
		event_app.init();
	});
</script>						