<input type="hidden" value="<?php echo $main_data['ris_id'] ?>" id="transaction_id">
 
						<div class="t-content">
							<div class="form-group" style="display:none">
					  			<div class="control-label">Company Name</div>
					  			<select name="" id="create_project" class="form-control input-sm"></select>
					  		</div>

					  		<div class="form-group" style="display:none">
					  			<div class="control-label">Project </div>
					  			<select name="" id="create_profit_center" class="select2" style="width:100%;" disabled="disabled"></select>					  			
				  			</div>

					  		<input type="hidden" id="account_code">			
					  		<input type='text' name="" id="to" class="form-control input-sm" style="display:none">
					  		<input type="hidden" id="projectid" value="<?php echo $this->session->userdata('Proj_Code')?>">							
							<div class="t-header">
								<h4><?php echo $main_data['risNo']; ?></h4>
							</div>
							
							<div class="row">
								<div class="col-xs-5">
									<div class="control-group">										
										<?php if(strtoupper($main_data['ris_status'])=="ACTIVE"): ?>
										<a href="<?php echo base_url().index_page();?>/print/ris/<?php echo $main_data['risNo']; ?>" target="_blank" class="action-status"><i class="fa fa-print"></i> Print</a>
										<a class="action-status history-link" data-method="ris" data-type="edit" data-value="<?php echo $main_data['risNo']; ?>" href="<?php echo base_url().index_page();?>/transaction_list/ris/<?php echo $main_data['risNo']; ?>/edit" target="_blank" class="action-status"><i class="fa fa-pencil"></i> Edit</a>
										<?php else: ?>
										<a href="<?php echo base_url().index_page();?>/print/ris_gate_pass/<?php echo $main_data['risNo']; ?>" target="_blank" class="action-status"><i class="fa fa-print"></i> Print Gate Pass</a>
											<?php
												if($this->lib_auth->restriction('USER'))
												{
													
													if(strtoupper($main_data['ris_status'])!='APPROVED'){
														echo $this->lib_transaction->status2($main_data['ris_status'],$main_data['risNo']);
													}else{
														echo "<span class='label label-success'>".$main_data['ris_status']."</span>";
													}
													
												}										
											?>
										<?php endif; ?>
									</div>
								</div>
								
								<div class="col-md-5">
									
									<?php 						
									
										$branch_type = $this->session->userdata('branch_type');										
										$where = '';									
										switch($branch_type){
											case "MAIN OFFICE":
																								

													if($this->lib_auth->restriction('USER'))
													{
														echo $this->lib_transaction->ris_status($main_data['ris_status'],$main_data['ris_id']);
													}

													/*if($this->lib_auth->restriction('CANVASS USER'))
													{
														if(strtoupper($main_data['status'])=='APPROVED'){
															echo $this->extra->label($main_data['status']);	
														}	
													}*/
														
													

											break;
											case "PROFIT CENTER":
												echo $this->extra->label($main_data['ris_status']);									
											break;
											default:
													
											break;										
										}
																
								  ?>
								  
								</div>
							</div>
													
							<div class="row" style="margin-top:10px">
								<div class="col-xs-6">
									<div class="t-title">
										<div>Created From : </div> 
										<strong><?php echo $main_data['project_requestor']; ?></strong>
									</div>
									<div class="t-title">
										<div>Date: </div>
										<strong><p><?php echo $main_data['risDate'] ?></p></strong>
									</div>
									<div class="t-title">
										<div>Remarks : </div>
										<strong><p><?php echo $main_data['ris_remarks'] ?></p></strong>
									</div>
									
								</div>
							</div>
							
							<div class="table-responsive" style="overflow:auto">
							<table class="table table-item long_item">
								<thead>
									<tr>		
										<th>Item Description</th>
										<th>Qty</th>
										<th>Unit Cost</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$total = 0;
										foreach ($details_data as $row){ 
											$total = $total + ($row['qty'] * $row['unit_cost']);
									?>
										<tr>
											<td><?php echo $row['itemDesc'];?>
											</td>
											<td class="td-number" style="text-align:left;"><!-- <input type="number" class="number_only" value="<?php echo number_format($row['qty'],2);?>" name="ris_qty" id="ris_qty" class="form-control input-sm numbers_only" style="width: 20%;"> -->
												<?php echo number_format($row['qty'],2); ?>
											</td>	
											<td class="td-number" id="ris_unitCost" style="text-align:left;"><?php echo number_format($row['unit_cost'],2);?></td>																			
											<td class="td-number" style="text-align:left;"><?php echo number_format($row['unit_cost'] * $row['qty'],2);?></td>
										</tr>
									<?php 
										} 
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><?php echo count($details_data); ?> item(s)</td>
										<td></td>
										<td></td>
										<td class="td-number" style="text-align:right;font-weight:bold;">TOTAL: <?php echo number_format($main_data['total_cost'],2);?></td>
									</tr>
								</tfoot>
							</table>
							</div>							
							<div class="row">
								<div class="col-xs-6">
									<div class="t-title">
										<div>Prepared By : </div> 
										<strong><?php echo $main_data['preparedBy_name']; ?></strong>										
									</div>
								</div>
								<div class="col-xs-6">
									<div class="t-title">
										<div>Approved By : </div> 
										<?php if(!empty($main_data['approvedBy_name'])){ ?>
										<strong id="approve_exist" data-id="<?php echo $main_data['approvedBy']; ?>"><?php echo $main_data['approvedBy_name']; ?></strong>
										<!-- <strong id="approve_id"><?php echo $main_data['approvedBy']; ?></strong> -->
										<?php }else{ ?>
										<select name="approved_by" id="approved_by" class="form-control input-sm" style="width:350px;"></select>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="t-title">
										<div>Checked By : </div> 
										<strong><?php echo $main_data['checkedBy_name']; ?></strong>										
									</div>
								</div>
							</div>
						</div>



<script>
	$(function(){
		var event_app = {
			init:function(){

				var option = {
					profit_center : $('#create_profit_center'),
					main_office : true,
				}

				$('#create_project').get_projects(option);

				var profit_center_value = '';
				if(typeof ($('#create_profit_center option:selected').val()) != 'undefined'){
					 profit_center_value = $('#create_profit_center option:selected').val();
				}

				$.signatory({
					type          : 'ris',
					approved_by   : ["4", "4", "1",profit_center_value],
				});

				this.bindEvent();
			},
			bindEvent:function(){

				$('.pending-event').on('click',this.pending_event);
				$('.approved-event').on('click',this.approved_event);
				$('.reject-event').on('click',this.reject_event);
				$('.cancel-event').on('click',this.cancel_event);
				
			},pending_event:function(){

				$post = {
					id     : $('#transaction_id').val(),
					transaction_no : "<?php echo $main_data['risNo']; ?>",
					status : 'PENDING',
					type   : 'ris',
					remarks: '',
					approve: $('#approved_by option:selected').val()
				};
				//event_app.execute_query($post);

			},approved_event:function(){

				if($('.required').required()){
					return false;
				}
				
				var bool = confirm('Are you sure?');
				if(!bool){
					return false;
				}
				var item_container = new Array();
				$('.approved-qty').each(function(i,val){

					var item = {
						qty    : $(val).val(),
						itemNo : $(val).attr('data-itemno'),
					}
					// alert();
					item_container.push(item);
				});
				
				var approved_by_exist= $('#approve_exist').text();
				if(approved_by_exist == ''){
					approved_by_exist = $('#approved_by option:selected').val();
				}

				var $post = {
					'id'		: $('#transaction_id').val(),
					'status'	: 'APPROVED',
					'transaction_no': "<?php echo $main_data['risNo']; ?>",
					'type'		: 'RIS',
					'remarks'	: '',
					'approve'	: approved_by_exist,
					'approve_id': $('#approve_exist').attr('data-id')
					// 'qty'		: $('#ris_qty').val()
				};

				event_app.execute_query($post);
				
			},reject_event:function(){
				var bool = confirm('Are you sure?');
				if(!bool){
					return false;
				}

				var remarks = prompt('Reason for Rejection');
					
				if (remarks == '') {
					alert('Please add reason');
					return false;
				}

				var approved_by_exist= $('#approve_exist').text();
				if(approved_by_exist == ''){
					approved_by_exist = $('#approved_by option:selected').val();
				}
				$post = {
					id     : $('#transaction_id').val(),					
					status : 'REJECTED',
					transaction_no : "<?php echo $main_data['risNo']; ?>",
					remarks: remarks,
					type   : 'RIS',
					approve: approved_by_exist
				};
				
				event_app.execute_reject($post);

			},cancel_event:function(){
				var bool = confirm('Are you sure?');
				if(!bool){
					return false;
				}

				$post = {
					id     : $('#transaction_id').val(),					
					status : 'CANCEL',
					transaction_no : "<?php echo $main_data['risNo']; ?>",
					remarks: '',
					type   : 'RIS',
					approve: $('#approved_by option:selected').val()
				};				
				event_app.execute_query($post);

			},execute_query:function($post){
					$.post('<?php echo base_url().index_page();?>/transaction/change_status',$post,function(response){
					switch($.trim(response)){
						case "1":						
							alert('Successfully Updated');
							/*updateContent();
							updateStatus($post.status)*/;
							
							$.fancybox.close();
							location.reload('true');
						break;
					}
					
				});
			},execute_reject:function($post){
					$.post('<?php echo base_url().index_page();?>/transaction/ris_change',$post,function(response){
						// switch($.trim(response)){
						// 	case "1":						
								alert('Successfully Updated');
								/*updateContent();
								updateStatus($post.status)*/;
								
								$.fancybox.close();
								location.reload('true');
						// 	break;
						// }
						
				});
			}

		}
		event_app.init();
	});
</script>						