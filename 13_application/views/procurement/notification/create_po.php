
<input type="hidden" value="" class="date" id="po_date">
<input type="hidden" value="" class="" id="po_no">
<input type="hidden" value="<?php echo $canvass_main['pr_id']; ?>" id="pr_id">
<input type="hidden" value="<?php echo $canvass_main['can_id'] ?>" id="can_id">

<input type="hidden" value="<?php echo $supplier_info['supplier_id'] ?>" id="supplier_id">
<input type="hidden" value="BUSINESS" id="supplier_type">



<div class="content-title">
	<h3>Create P.O</h3>	
</div>

<div class="row">
	<div class="col-md-12">
		<div class="t-content">			
			<div class="row">
				<div class="col-md-2">
					<div class="panel panel-default">
						<div class="panel-heading">
							Canvass Supplier
						</div>
						<table class="table">
							<tbody>
								<?php foreach($supplier as $row): ?>
								<?php 
									$supplier_id = $this->uri->segment(4);
									$active = ($row['supplier_id'] == $supplier_id )? 'act': '';
								 ?>
											<tr class="<?php echo $active; ?>">
												<td>
													<?php if(!empty($row['po_id'])): ?>
														<label  class="label label-success">Already PO</label>
													<?php endif; ?>
													<a class="supplier" href="<?php echo base_url().index_page(); ?>/transaction/create_po/<?php echo $canvass_main['c_number']; ?>/<?php echo $row['supplier_id']; ?>"><?php echo $row['Supplier'] ?></a></td>
													<input type="hidden" id="vat" value="<?php echo $row['vat'];?>" />
													<input type="hidden" id="vat_percentage" value="<?php echo $row['vat_percentage'];?>" />
													<input type="hidden" id="nature_expense" value="<?php echo $row['nature_expense'];?>" />
													<input type="hidden" id="nature_percentage" value="<?php echo $row['nature_percentage'];?>" />
											</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-10">
					
					<div class="t-header">
						<a href="<?php echo base_url().index_page(); ?>/transaction_list/purchase_order/for_po" class="close"><span aria-hidden="true">&times;</span><span></a>
						<!-- <h4 id="po-no-title">-</h4> -->
					</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group" hidden="hidden">
								<?php foreach($pr_details as $row){?>
					  			<div class="control-label">Account Name (DEBIT)</div>
					  			 <input type="text" name="" id="accountname" style="width:100%" placeholder="Account Name" data-id="<?php echo $row['account_id']; ?>" data-code="<?php echo $row['account_code'];?>" data-desc="<?php echo $row['account_name']; ?>" data-subid="<?php echo $row['subsidiary_id'];?>" data-subdesc="<?php echo $row['subsidiary_name'];?>" >
					  			<?php }?>
					  		</div>
					  		
							 <div class="t-title">
							 	<div>Reference No : </div>
							 	<input type="text" class="form-control required" id="reference_no" placeholder="" value="<?php echo $pr_main['from_projectNo'].'-'.$reference_no; ?>">
							 </div>
							
							 <div class="t-title">
							 	<div>Date of Delivery : </div>
							 	<input type="text" class="form-control required" id="date_of_delivery">
							 </div>

							 <div class="t-title">
							 	<div>Place of Delivery : </div>
							 	<input type="text" class="form-control required uppercase" id="place_delivery" >
							 </div>
								
							  <div class="t-title">
							 	<div>Delivery Terms : </div>
							 	<div class="row">
							 		<div class="col-md-6">
							 			<select name="" class="form-control" id="terms_payment">
							 				<option value="COD">COD</option>
							 				<option value="In Days">In Days</option>
							 			</select>
							 		</div>
							 		<div class="col-md-6">
							 			<input type="number" id="in_days"  value="1" class="form-control">
							 		</div>
							 	</div>					 	
							 </div>
							 <div class="t-title">
							 	<div>Remarks : </div>
							 	<input type="text" class="form-control required uppercase" id="remarks">		 	
							 </div>
							  <div class="t-title">
							 	<div>Mode of Procurement : </div>
							 	<input type="text" class="form-control required uppercase" id="mode-proc" >
							 </div>
						</div>
						<div class="col-md-3">
							<div class="control-group">						
								<div class="t-title">
									<div>Supplier :</div>
									<strong><?php echo $supplier_info['business_name']; ?></strong>
								</div>
								<div class="t-title">
									<div>Address :</div>
									<strong><?php echo $supplier_info['address']; ?></strong>
								</div>
								<div class="t-title">
									<div>Contact No :</div>
									<strong><?php echo $supplier_info['contact_no']; ?></strong>
								</div>					
							</div>
						</div>
						<div class="col-md-2">
							
							<div class="t-title">
								<div>Canvass No :</div>
								<strong><?php echo $canvass_main['c_number']; ?></strong>
							</div>
							<div class="t-title">
								<div>Canvass Date :</div>
								<strong><?php echo $this->extra->format_date($canvass_main['c_date']); ?></strong>
							</div>					
						</div>
						<div class="col-md-2">
							<div class="t-title">
								<div>P.R No :</div>
								<strong><?php echo $pr_main['purchaseNo']; ?></strong>
							</div>
							<div class="t-title">
								<div>P.R Date :</div>
								<strong><?php echo $this->extra->format_date($pr_main['purchaseDate']); ?></strong>
							</div>
							<div class="t-title">
								<div>Created From :</div>
								<strong><?php echo $pr_main['from_projectCodeName']; ?></strong>								
							</div>							
						</div>					
					</div>
					<div class="table-responsive">				
					<table id="tbl_pr_details" class="table table-item">
						<thead>
							<tr>
								<th style="width:40px">Change Order?</th>
								<th>Item Name</th>
								<th>Unit</th>
								<th class="td-number td-qty">Qty</th>
								<th class="td-number td-qty">Unit Price</th>
								<th class="td-number">Total</th>
								<th>Project type</th>
								<th>PR Remarks</th>
								<th>PO Remarks</th>
							</tr>
						</thead>
						<tbody class="tbl_details">
							<?php 
								$grand_total = 0;
								if(count($canvass_details) > 0):
								foreach($canvass_details as $row): 
							?>
								<tr>
									<td><input type="checkbox" class="change_order"></td>
									<td class="item-desc"><?php echo $row['itemName']; ?></td>
									<td class="item-no" style="display:none"><?php echo $row['itemNo']; ?></td>
									<td class="unit"><?php echo $row['unit_measure']; ?></td>
									
									<td class="td-number td-qty "><span class="editable qty"><?php echo $this->extra->comma($row['pr_qty']); ?></span></td>
									<td class="td-number unit-price"><?php echo $this->extra->number_format($row['discounted_price']); ?></td>
									<!-- <td class="td-number td-qty precentage"><?php echo $row['percentage']; ?></td> -->
									<!-- <td class="td-number td-qty discounted_price"><?php echo $row['discounted_price']; ?></td> -->
									<?php
										$total = $row['pr_qty'] * $row['discounted_price'];
										$grand_total += $total;
									?>
									<td class="td-number total"><?php echo $this->extra->number_format($total); ?></td>
									<td class="for_usage"><?php echo $row['for_usage']; ?></td>
									<td><?php echo $row['remarkz']; ?></td>
									<td class="remarks editable_remarks"><?php echo (empty($row['c_remarks']))? '-': $row['c_remarks']; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
								<tr>
									<td colspan="7">Empty</td>
								</tr>
						<?php endif; ?>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td><?php echo count($canvass_details); ?> item(s)</td>
								<td></td>
								<td></td>	
								<td></td>
								<td class="td-number td-qty">Total : <span id="grand_total"><strong><?php echo $this->extra->number_format($grand_total);  ?></strong></span></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
					</div>
					

					<div class="row">
						<div class="col-md-3">
							<div class="t-title">
								<div>Requested by : </div>
								<select name="" id="prepared_by" class="form-control"></select>
							</div>
							<!--<div class="t-title">
								<div>Checked by : </div>
								<select name="" id="checked_by" class="form-control"></select>
							</div>-->					
						</div>

						<div class="col-md-3">
							<div class="t-title">
								<div>Approved By : </div>
								<select name="" id="approved_by" class="form-control"></select>
							</div>
						</div>
					</div>

					<?php echo $this->load->view('procurement/ris/journal_entries_view');?>

					<div class="row">
						<div class="col-md-12">
							<button id="save" class="btn btn-primary pull-right">Save</button>
						</div>
					</div>

				</div>
			</div>
		</div>		
	</div>
</div>

<script>
		var xhr = "";
		var po = {
			init:function(){

				$('#date_of_delivery').date();
				$('#po_date').date({
					url   : 'get_po_code',
					dom   : $('#po_no'),
					div   : $('#po-no-title'),
					event : 'change',
				});

				var cmbproject = '<?php echo $this->session->userdata("Proj_Code"); ?>';

				$.signatory({
					type           : 'po',
					approved_by    : ["6","4","1",cmbproject],
					checked_by     : ["6","5","1",cmbproject],
				});

				$(".editable").editable("<?php echo base_url().index_page(); ?>/ajax/display",{ 					      
					      tooltip   : "Click to edit...",
					      width     : $('.editable').width() + 50,
					      callback  : function(value, settings){							
							var unit = remove_comma($(this).closest('tr').find('.unit-price').text());
							var total = parseFloat(value) * parseFloat(unit);
							$(this).closest('tr').find('.total').html(comma(total.toFixed(2)));
							var grand_total = 0;
							$('#tbl_pr_details tbody tr').each(function(i,val){
								var total = remove_comma($(val).find('.total').text());								
								grand_total +=parseFloat(total);
							});
							$('#grand_total').html(comma(grand_total.toFixed(2)));
					      }
				});

				$(".editable_remarks").editable("<?php echo base_url().index_page(); ?>/ajax/display",{ 					      
					      tooltip   : "Click to edit...",
					      width     : $('.editable_remarks').width() + 50,					  
				});

				this.bindEvent();

				var g_total = $('#grand_total').text();

				g_total = g_total.replace(',','');

				$('#tbl_pr_details tbody tr').each(function(i,val){
					var account_code = $('#account_name').attr('data-code');
					var account_id = $('#account_name').attr('data-id');
					var account_name = $('#account_name').attr('data-desc');
					var account_default = $('#account_name').attr('data-default');
					var accountcode = $('#accountname').attr('data-code');
					var accountid = $('#accountname').attr('data-id');
					var accountname = $('#accountname').attr('data-desc');
					var accountdefault = $('#accoutnname').attr('data-default');
					var unit_cost = $(val).find('.unit-price').text();
					var qty = $(val).find('.qty').text();
					var item_id = $(val).find('.item-no').text();

					// alert(item_id);
					unit_cost = unit_cost.replace(',','');
					qty = qty.replace(',','');
					var amount = parseFloat(qty) * parseFloat(unit_cost);

		                $get = {
		                	title: account_name
		                };
		                 $.getJSON('<?php echo site_url('procurement/purchase_request/get_account_subsidiary')?>',$get,function(json){
		                	$('#subsid option:gt(0)').remove();
		                	$('#subsid option:first').val('0');
		                	$('#subsid option:first').text('Select Item');
		                	for(var i = 0; i < json.length; i++){
			                	$('<option/>').attr('value',json[i]['id']).attr('data-accountid',json[i]['account_id']).attr('data-desc',json[i]['description']).attr('data-id', json[i]['id']).text(json[i]['description']).appendTo($('#subsid'));
			                }
		                });
				var sub_id = $('#subsid option:selected').attr('data-id');
				var sub_name = $('#subsid option:selected').attr('data-desc');
				var subsidiaryid = $('#account_ame').attr('data-subid');
				var subsidiaryname = $('#accountname').attr('data-subdesc');
				// alert(subsidiaryid);
				// alert(subsidiaryname);
				// return false;

					if(accountid != 0 || accountid != '0'){
						$div =  '<tr>' 
	                            +'<td><span class="close padding-right pull-left remove" data-itemid="'+item_id+'">&times;</span></td>'
	                            +'<td class="editable_code pointer">'+accountcode+'</td>'
	                            +'<td class="editable_journal pointer" data-account_id="'+accountid+'" data-ledger="" data-code="'+accountcode+'" data-taccount="DEBIT">'+accountname+'</td>'
	                            +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
	                            +'<select class="subsidiary_select">'
	                            +'<option value="'+subsidiaryid+'">'+subsidiaryname+'</option>'
	                            +'</select>'
	                            +'</td>'
	                            +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" style="width:100px;" value="'+comma(round2Fixed(amount))+'" /></td>'
	                            +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="0.00" style="width:100px;" /></td>'
	                            +'<td style="display:none;">MATSUP INVENTORY</td>'
	                            +'<td class="default" style="display:none;">DEBIT</td>'
	                            +'</tr>';               
	                    $('.bill_journal_contents').append($div);
					}

					journal_editable();
					get_total();
				});
				
				g_total = remove_comma(g_total);

				//vat entries computation
				var vat = $('#vat').val();
				var vat_percentage = $('#vat_percentage').val();
				var nature_expense = $('#nature_expense').val();
				var nature_percentage = $('#nature_percentage').val();

				var total_amount = g_total;
                var input_vat = parseFloat(total_amount) / 1.12 * (parseFloat(vat_percentage) / 100);
                var net_vat = parseFloat(total_amount) - parseFloat(input_vat);

                var expanded_tax = parseFloat(total_amount) / 1.12 * (parseFloat(nature_percentage) / 100);
                var ap_supplier = parseFloat(total_amount) - parseFloat(expanded_tax) - parseFloat(input_vat);
                var total_credit = parseFloat(ap_supplier) + parseFloat(expanded_tax);

                $.getJSON('<?php echo base_url().index_page();?>/procurement/purchase_order/get_vat',{vat:vat},function(json){
                	for(var i = 0; i < json.length; i++){
                		if(json[i]['type'] == 'due_to_bir_withholding_gmp_tax'){
	            			var account_id = json[i]['account_id'];
	            			var account_code = json[i]['code'];
	            			var account_name = json[i]['account_name'];
	            			var subsidiary_id = json[i]['subsidiary_id'];
	            			var subsidiary_name = json[i]['subsidiary_name'];

	            			$div =  '<tr>' 
		                            +'<td><span class="close padding-right pull-left remove" data-itemid="">&times;</span></td>'
		                            +'<td class="editable_code pointer">'+account_code+'</td>'
		                            +'<td class="editable_journal pointer" data-account_id="'+account_id+'" data-ledger="" data-code="'+account_code+'" data-taccount="CREDIT">'+account_name+'</td>'
		                            +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
		                            +'<select class="subsidiary_select">'
		                            +'<option value="'+subsidiary_id+'" selected="selected">'+subsidiary_name+'</option>'
		                            +'</select>'
		                            +'</td>'
		                            +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" style="width:100px;" value="0.00" /></td>'
		                            +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="'+comma(round2Fixed(input_vat))+'" style="width:100px;" /></td>'
		                            +'<td style="display:none;">MATSUP INVENTORY</td>'
		                            +'<td class="default" style="display:none;">CREDIT</td>'
		                            +'</tr>';               
		                    $('.bill_journal_contents').append($div);
	            		}else if(json[i]['type'] == 'due_to_bir_expanded_withholding_tax'){
	            			var account_id = json[i]['account_id'];
	            			var account_code = json[i]['code'];
	            			var account_name = json[i]['account_name'];
	            			var subsidiary_id = json[i]['subsidiary_id'];
	            			var subsidiary_name = json[i]['subsidiary_name'];

	            			$div =  '<tr>' 
		                            +'<td><span class="close padding-right pull-left remove" data-itemid="">&times;</span></td>'
		                            +'<td class="editable_code pointer">'+account_code+'</td>'
		                            +'<td class="editable_journal pointer" data-account_id="'+account_id+'" data-ledger="" data-code="'+account_code+'" data-taccount="CREDIT">'+account_name+'</td>'
		                            +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
		                            +'<select class="subsidiary_select">'
		                            +'<option value="'+subsidiary_id+'" selected="selected">'+subsidiary_name+'</option>'
		                            +'</select>'
		                            +'</td>'
		                            +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" style="width:100px;" value="0.00" /></td>'
		                            +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="'+comma(round2Fixed(expanded_tax))+'" style="width:100px;" /></td>'
		                            +'<td style="display:none;">MATSUP INVENTORY</td>'
		                            +'<td class="default" style="display:none;">CREDIT</td>'
		                            +'</tr>';               
		                    $('.bill_journal_contents').append($div);
	            		}else if(json[i]['type'] == 'payable'){
	            			var account_id = json[i]['account_id'];
	            			var account_code = json[i]['code'];
	            			var account_name = json[i]['account_name'];
	            			var subsidiary_id = json[i]['subsidiary_id'];
	            			var subsidiary_name = json[i]['subsidiary_name'];

	            			$div =  '<tr>' 
		                            +'<td><span class="close padding-right pull-left remove" data-itemid="">&times;</span></td>'
		                            +'<td class="editable_code pointer">'+account_code+'</td>'
		                            +'<td class="editable_journal pointer" data-account_id="'+account_id+'" data-ledger="" data-code="'+account_code+'" data-taccount="CREDIT">'+account_name+'</td>'
		                            +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
		                            +'<select class="subsidiary_select">'
		                            +'<option value="'+subsidiary_id+'">'+subsidiary_name+'</option>'
		                            +'</select>'
		                            +'</td>'
		                            +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" style="width:100px;" value="0.00" /></td>'
		                            +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="'+comma(round2Fixed(ap_supplier))+'" style="width:100px;" /></td>'
		                            +'<td style="display:none;">MATSUP INVENTORY</td>'
		                            +'<td class="default" style="display:none;">CREDIT</td>'
		                            +'</tr>';               
		                    $('.bill_journal_contents').append($div);
	            		}
                	}

                	get_total();
                });

				/*$divd =  '<tr>' 
                        +'<td><span class="close padding-right pull-left remove" data-itemid="">&times;</span></td>'
                        +'<td class="editable_code pointer">20101010</td>'
                        +'<td class="editable_journal pointer" data-account_id="555" data-ledger="" data-code="20101010" data-taccount="CREDIT">Accounts Payable</td>'
                        +'<td class="editable_subsidiary pointer" data-subsidiary_id="">'
                        +'<select class="subsidiary_select">'
                        +'<option value="">Office Supplies and Expenses</option>'
                        +'</select>'
                        +'</td>'
                        +'<td class="editable_amount"><input type="text" class="debit pointer" placeholder="0.00" style="width:50px;" value="0.00" /></td>'
                        +'<td class="editable_amount"><input type="text" class="credit pointer" placeholder="0.00" value="'+comma(round2Fixed(g_total))+'" style="width:50px;" /></td>'
                        +'<td style="display:none;">MATSUP INVENTORY</td>'
                        +'<td class="default" style="display:none;">CREDIT</td>'
                        +'</tr>';               
                $('.bill_journal_contents').append($divd);*/

                $('.total_credit').html(comma(round2Fixed(g_total)));
			},
			bindEvent:function(){
				$('#supplier').on('change',this.po_items);
				$('#supplier').trigger('change');
				$('#save').on('click',this.save);
			},
			po_items:function(){
				$post = {
					can_id : $('#supplier option:selected').attr('data-can_id'),
					supplier_id : $('#supplier option:selected').val(),
				}
				$.post('<?php echo base_url().index_page();?>/transaction/get_po_items',$post,function(response){
					$('#po_items').html(response);
				});

				//vat computation
			},save:function(){

		  		$('.bill_journal_contents tr').each(function(i,val){             
		            if($(val).find('.editable_journal').attr('data-account_id') > 0){
		                check_journal = false;
		            }else{
		                check_journal = true;
		            }
		        });
				if($('.required').required()){
					return false;
				}
								
				var bool = confirm('Are you sure?');

				if(!bool){
					return false;
				}

				var details = new Array();
				var details_main = new Array();
				$('#tbl_pr_details tbody tr').each(function(i,val){

					details = {
						itemNo   : $(val).find('.item-no').text(),
						itemDesc : $(val).find('.item-desc').text(),
						unit     : $(val).find('.unit').text(),
						qty      : remove_comma($(val).find('.qty').text()),
						unit_price : remove_comma($(val).find('.unit-price').text()),
						total    : remove_comma(($(val).find('.total').text())),
						remarks  : $(val).find('.remarks').text(),
						change_order : $(val).find('.change_order').is(':checked'),
						for_usage : $(val).find('.for_usage').text(),
					}					
					details_main.push(details);
					details = [];

				});

				var journal_details = new Array();

		        $('.bill_journal_contents tr').each(function(i,val){
		            var obj = {
		                account_code : $(val).closest('tr').find('td.editable_code').text(),
		                account_id : $(val).closest('tr').find('td.editable_journal').attr('data-account_id'),
		                account_title : $(val).closest('tr').find('td.editable_journal').text(),
		                subsidiary_id : $(val).closest('tr').find('.subsidiary_select option:selected').val(),
		                subsidiary : $(val).closest('tr').find('.subsidiary_select option:selected').text(),
		                debit_amount : $(val).closest('tr').find('.debit').val(),
		                credit_amount : $(val).closest('tr').find('.credit').val(),
		                type : $(val).closest('tr').find('td:eq(6)').text(),
		                def : $(val).closest('tr').find('td.editable_journal').attr('data-taccount')
		            }
		            journal_details.push(obj);
		        });
				
				$post = {
						can_id		  : $('#can_id').val(),
						po_number     : $('#po_no').val(),
						po_date       : $('#po_date').val(),
						reference_no  : $('#reference_no').val(),
						pr_id         : $('#pr_id').val(),
						supplierID    : $('#supplier_id').val(),
						supplierType  : $('#supplier_type').val(),
						placeDelivery : $('#place_delivery').val(),
						deliverTerm   : $('#delivery_term').val(),
						paymentTerm   : $('#terms_payment option:selected').val(),
						indays        : $('#in_days').val(),
						dtDelivery    : $('#date_of_delivery').val(),
						approvedBy    : $('#approved_by').val(),
						preparedBy    : $('#prepared_by').val(),					
						recommendedBy : $('#checked_by').val(),
						title_id      : '<?php echo $this->session->userdata("Proj_Main"); ?>',
						project_id    : '<?php echo $this->session->userdata("Proj_Code"); ?>',
						po_remarks    : $('#remarks').val(),
						from_id       : '<?php echo $pr_main["from_projectCode"] ?>',
						from_name     : '<?php echo $pr_main["from_projectCodeName"] ?>',
						data          : details_main,
						journal_details : JSON.stringify(journal_details),
						mode_procurement		: $('#mode-proc').val()
				}
				$.save({appendTo : 'body'});

		        if(xhr && xhr.readystate != 4){
		            xhr.abort();		            
		        }
								
				xhr = $.post('<?php echo base_url().index_page();?>/procurement/purchase_order/save_purchaseOrder',$post,function(response){
					switch($.trim(response)){
						case "1":
							alert('Successfully Save');
							$.save({action : 'success',reload : 'true'});												
						break;
						case "2":							
							alert('Already Save!');
							$.save({action : 'error',reload : 'true'});							
						break;
						default:
							alert(response);
							$.save({action : 'error',reload : 'true'});		
						break;
					}
				});
				
				

			}
		}

	$(function(){
		po.init();
	});

	var get_total = function(){

    var total_debit  = 0;
    var total_credit = 0;
    $('.bill_journal_contents tr').each(function(i,val){
            var debit  = remove_comma($(val).closest('tr').find('.debit').val());
            var credit = remove_comma($(val).closest('tr').find('.credit').val());
            total_debit  = +total_debit + +debit;
            total_credit = +total_credit + +credit;
    });

    $('.total_debit').html(comma(round2Fixed(total_debit)));
    $('.total_credit').html(comma(round2Fixed(total_credit)));

}
</script>