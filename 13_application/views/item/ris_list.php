
<table class="table myTable">
<thead>
	<tr>
		<th>No</th>
		<th>RIS No</th>
		<th>RIS Date</th>
		<th>Stock No</th>
		<th>QTY</th>
		<th>Unit</th>
		<th>Description</th>
		<th>Account Code</th>
		<th>Unit Price</th>
		<th>Total Amount</th>
		<th>Purpose</th>
		<th>RIS STATUS</th>
	</tr>
</thead>	
<tbody>
	<?php $counter = 1;?>
	<?php foreach($pr_items as $row): ?>
	<tr>
		<td><?php echo $counter; ?></td>
		<td><?php echo $row['risNo']; ?></td>
		<td><?php echo $row['risDate']; ?></td>
		<td><?php echo $row['stock_code']; ?></td>
		<td><?php echo $row['qty']; ?></td>
		<td><?php echo $row['unitmeasure']; ?></td>
		<td><?php echo $row['itemDesc']; ?></td>
		<td><?php echo $row['account_code'];?></td>
		<td><?php echo number_format($row['unit_cost'],2); ?></td>
		<td><?php echo number_format($row['total_unitcost'],2); ?></td>
		<td><?php echo $row['purpose']; ?></td>
		<td><?php echo $row['ris_status'];?></td>
	</tr>
	<?php $counter++;?>
	<?php endforeach; ?>
</tbody>
</table>