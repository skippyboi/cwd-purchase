
<table class="table myTable">
<thead>
	<tr>
		<th>No</th>
		<th>Reciept No</th>
		<th>Return Date</th>
		<th>QTY</th>
		<th>Unit</th>
		<th>Description</th>
		<th>Account Code</th>
		<th>Unit Price</th>
		<th>Total Amount</th>
		<th>Status</th>
	</tr>
</thead>	
<tbody>
	<?php $counter = 1;?>
	<?php foreach($pr_items as $row): ?>
	<tr>
		<td><?php echo $counter; ?></td>
		<td><?php echo $row['receipt_no']; ?></td>
		<td><?php echo $row['date_received']; ?></td>
		<td><?php echo $row['item_quantity_actual']; ?></td>
		<td><?php echo $row['unit_msr']; ?></td>
		<td><?php echo $row['item_name_actual']; ?></td>
		<td><?php echo $row['stock_code'];?></td>
		<td><?php echo $row['item_cost_actual']; ?></td>
		<td><?php echo number_format($row['total_unitcost'],2); ?></td>
		<td><?php echo $row['received_status']; ?></td>
	</tr>
	<?php $counter++;?>
	<?php endforeach; ?>
</tbody>
</table>