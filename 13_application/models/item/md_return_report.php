<?php defined('BASEPATH') OR exit('No direct script access allowed');

class md_return_report extends CI_Model {

	public function __construct(){
		parent :: __construct();		
		
	}



	function get_item($arg){
		$where = "";
		if($arg['location']!='all'){
			$where .= "AND pr.from_projectCode ='{$arg['location']}'";
		}

		if($arg['view_type'] == 'monthly'){

			$date = $arg['year']."-".$arg['month']."-01";
			$from = date('Y-m-01',strtotime($date));
			$to   = date('Y-m-t',strtotime($date));		

		}else{

			$from = $arg['date_from'];
			$to   = $arg['date_to'];

		}

		$where .= " AND a.risDate between '{$from}' AND '{$to}'";


		$sql = "
			SELECT
			a.risNo,
			a.risDate,
			-- (SELECT project_name FROM setup_project WHERE rs.`project_id` = setup_project.`project_id`) 'center',
			b.qty,
			b.unitmeasure,
			b.itemNo,
			b.itemDesc,
			b.unit_cost,
			(SELECT count(ris_id) FROM ris_details WHERE a.ris_id = b.ris_id) 'count',
			-- count(risNo) 'No',
			(SELECT item_code FROM setup_group_detail WHERE setup_group_detail.group_detail_id = b.itemNo) 'stock_code',
			(b.unit_cost * b.qty) 'total_unitcost',
			a.ris_id,
			b.account_code,
			a.ris_status
			FROM ris_main a
			INNER JOIN ris_details b
			 ON (a.ris_id = b.ris_id)
			WHERE (a.ris_status = 'APPROVED' OR a.ris_status = 'COMPLETE') 
			{$where}
		";

		$result = $this->db->query($sql);
		return $result->result_array();	

	}



}
