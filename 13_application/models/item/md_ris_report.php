<?php defined('BASEPATH') OR exit('No direct script access allowed');

class md_ris_report extends CI_Model {

	public function __construct(){
		parent :: __construct();		
		
	}



	function get_item($arg){
		$where = "";
		if($arg['location']!='all'){
			$where .= "AND pr.from_projectCode ='{$arg['location']}'";
		}

		if($arg['view_type'] == 'monthly'){

			$date = $arg['year']."-".$arg['month']."-01";
			$from = date('Y-m-01',strtotime($date));
			$to   = date('Y-m-t',strtotime($date));		

		}else{

			$from = $arg['date_from'];
			$to   = $arg['date_to'];

		}

		$where .= " AND a.risDate between '{$from}' AND '{$to}'";


		$sql = "
			SELECT
			a.purpose,
			a.risNo,
			a.risDate,
			-- (SELECT project_name FROM setup_project WHERE rs.`project_id` = setup_project.`project_id`) 'center',
			b.qty,
			b.unitmeasure,
			b.itemNo,
			b.itemDesc,
			b.unit_cost,
			a.ris_status,
			(SELECT count(ris_id) FROM ris_details WHERE a.ris_id = b.ris_id) 'count',
			-- count(risNo) 'No',
			(SELECT item_code FROM setup_group_detail WHERE setup_group_detail.group_detail_id = b.itemNo) 'stock_code',
			(b.unit_cost * b.qty) 'total_unitcost',
			a.ris_id,
			b.account_code,
			a.ris_status
			FROM ris_main a
			INNER JOIN ris_details b
			 ON (a.ris_id = b.ris_id)
			WHERE (a.ris_status = 'APPROVED' OR a.ris_status = 'COMPLETE' OR a.ris_status = 'REJECTED' OR a.ris_status = 'Active') 
			{$where}
		";
		$result = $this->db->query($sql);
		return $result->result_array();	

	}

	function get_return($arg){
		$where = "";
		if($arg['location']!='all'){
			$where .= "AND pr.from_projectCode ='{$arg['location']}'";
		}

		if($arg['view_type'] == 'monthly'){

			$date = $arg['year']."-".$arg['month']."-01";
			$from = date('Y-m-01',strtotime($date));
			$to   = date('Y-m-t',strtotime($date));		

		}else{

			$from = $arg['date_from'];
			$to   = $arg['date_to'];

		}

		$where .= " AND a.date_received between '{$from}' AND '{$to}'";


		$sql = "
			SELECT
			a.receipt_no,
			a.date_received,
			-- (SELECT project_name FROM setup_project WHERE rs.`project_id` = setup_project.`project_id`) 'center',
			b.item_quantity_actual,
			b.unit_msr,
			b.item_id,
			b.item_name_actual,
			b.item_cost_actual,
			a.received_status,
			(SELECT count(receipt_id) FROM return_details WHERE a.receipt_id = b.receipt_id) 'count',
			-- count(risNo) 'No',
			(SELECT item_code FROM setup_group_detail WHERE setup_group_detail.group_detail_id = b.item_id) 'stock_code',
			(b.item_cost_actual * b.item_quantity_actual) 'total_unitcost',
			a.receipt_id,
			a.received_status
			FROM return_main a
			INNER JOIN return_details b
			 ON (a.receipt_id = b.receipt_id)
			WHERE (a.received_status = 'APPROVED' OR a.received_status = 'COMPLETE' OR a.received_status = 'REJECTED' OR a.received_status = 'ACTIVE') 
			{$where}
		";
		$result = $this->db->query($sql);
		return $result->result_array();	

	}

	function get_receiving($arg){
		$where = "";
		if($arg['location']!='all'){
			$where .= "AND pr.from_projectCode ='{$arg['location']}'";
		}

		if($arg['view_type'] == 'monthly'){

			$date = $arg['year']."-".$arg['month']."-01";
			$from = date('Y-m-01',strtotime($date));
			$to   = date('Y-m-t',strtotime($date));		

		}else{

			$from = $arg['date_from'];
			$to   = $arg['date_to'];

		}

		$where .= " AND a.date_received between '{$from}' AND '{$to}'";


		$sql = "
			SELECT
			a.receipt_no,
			a.date_received,
			-- (SELECT project_name FROM setup_project WHERE rs.`project_id` = setup_project.`project_id`) 'center',
			b.item_quantity_actual,
			b.unit_msr,
			b.item_id,
			b.item_name_actual,
			b.item_cost_actual,
			a.received_status,
			(SELECT count(receipt_id) FROM return_details WHERE a.receipt_id = b.receipt_id) 'count',
			-- count(risNo) 'No',
			(SELECT item_code FROM setup_group_detail WHERE setup_group_detail.group_detail_id = b.item_id) 'stock_code',
			(b.item_cost_actual * b.item_quantity_actual) 'total_unitcost',
			a.receipt_id,
			a.received_status
			FROM receiving_main a
			INNER JOIN receiving_details b
			 ON (a.receipt_id = b.receipt_id)
			WHERE (a.received_status = 'APPROVED' OR a.received_status = 'COMPLETE' OR a.received_status = 'REJECTED' OR a.received_status = 'ACTIVE' OR a.received_status = 'PARTIAL') 
			{$where}
		";
		$result = $this->db->query($sql);
		return $result->result_array();	

	}
}
